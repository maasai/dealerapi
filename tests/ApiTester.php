<?php
abstract class ApiTester extends TestCase
{

    /**
     * Initialize
     */
    function __construct()
    {

    }

    public function setUp()
    {
        $_SERVER['REQUEST_METHOD'] = 'get';
        parent::setUp();
       // Artisan::call('migrate');
       // Artisan::call('db:seed');
    }

    /**
     * Gets JSON output from API
     * @param $uri
     * @param string $method
     * @param array $parameters
     * @return mixed
     */

    protected function getJson($uri, $method = 'GET', $parameters = [])
    {
        return json_decode($this->call($method, $uri, $parameters)->getContent());
    }


    protected function assertObjectHasAttributes()
    {
        $args = func_get_args();
        $object = array_shift($args);

        foreach ($args as $attribute) {
            $this->assertObjectHasAttribute($attribute, $object);
        }
    }

} 