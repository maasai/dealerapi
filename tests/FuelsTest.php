<?php 
class FuelsTest extends ApiTester{

    /**
     * @test
     */
    public function it_fetches_fuels()
    {
        $this->getJson('fuels');
        $this->assertResponseStatus(200);
    }
    /**
     * @test
     */
    public function it_fetches_single_fuel()
    {
        $fuel = $this->getJson('fuels/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_fuel()
    {
        $this->getJson('fuels/0');
        $this->assertResponseStatus(404);
    }
} 