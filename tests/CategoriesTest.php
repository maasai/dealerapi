<?php 
class CategoriesTest extends ApiTester{

    /**
     * @test
     */
    public function it_fetches_categories()
    {
        $this->getJson('categories');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_listing()
    {
        $category = $this->getJson('categories/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_listing()
    {
        $this->getJson('categories/0');
        $this->assertResponseStatus(404);
    }
}