<?php 
class MakesTest extends ApiTester{
    /**
     * @test
     */
    public function it_fetches_makes()
    {
        $this->getJson('makes');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_make()
    {
        $make = $this->getJson('makes/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_make()
    {
        $this->getJson('makes/0');
        $this->assertResponseStatus(404);
    }
}