<?php 
class FeaturesTest extends ApiTester{
    /**
     * @test
     */
    public function it_fetches_features()
    {
        $this->getJson('features');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_feature()
    {
        $feature = $this->getJson('features/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_feature()
    {
        $this->getJson('features/0');
        $this->assertResponseStatus(404);
    }
} 