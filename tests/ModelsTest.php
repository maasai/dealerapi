<?php 
class ModelsTest extends ApiTester{
    /**
     * @test
     */
    public function it_fetches_models()
    {
        $this->getJson('models');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_model()
    {
        $model = $this->getJson('models/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_model()
    {
        $this->getJson('models/0');
        $this->assertResponseStatus(404);
    }
}