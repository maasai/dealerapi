<?php 
class ConditionsTest extends ApiTester{

    /**
     * @test
     */
    public function it_fetches_conditions()
    {
        $this->getJson('conditions');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_condition()
    {
        $condition = $this->getJson('conditions/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_condition()
    {
        $this->getJson('conditions/0');
        $this->assertResponseStatus(404);
    }
} 