<?php 
class TransmissionsTest extends ApiTester{

    /**
     * @test
     */
    public function it_fetches_transmissions()
    {
        $this->getJson('transmissions');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_transmission()
    {
        $transmission = $this->getJson('transmissions/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_transmission()
    {
        $this->getJson('transmissions/0');
        $this->assertResponseStatus(404);
    }
} 