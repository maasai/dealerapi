<?php 
class ColorsTest extends ApiTester{
    /**
     * @test
     */
    public function it_fetches_colors()
    {
        $this->getJson('colors');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_color()
    {
        $color = $this->getJson('colors/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_color()
    {
        $this->getJson('colors/0');
        $this->assertResponseStatus(404);
    }
}