<?php
class ListingsTest extends ApiTester{
    /**
     * @test
     */
    public function it_fetches_listings()
    {
        $this->getJson('listings');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_listing()
    {
        $listing = $this->getJson('listings/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_listing()
    {
        $this->getJson('listings/0');
        $this->assertResponseStatus(404);
    }
}