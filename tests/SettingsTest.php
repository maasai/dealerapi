<?php 
class SettingsTest extends ApiTester{
    /**
     * @test
     */
    public function it_fetches_settings()
    {
        $this->getJson('settings');
        $this->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_fetches_single_setting()
    {
        $setting = $this->getJson('settings/18581');
        $this->assertResponseStatus(404);
    }
    /**
     * @test
     */
    public function it_fetches_not_exist_setting()
    {
        $this->getJson('settings/0');
        $this->assertResponseStatus(404);
    }
} 