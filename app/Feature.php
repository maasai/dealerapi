<?php namespace App;


class Feature extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'features';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array('uuid','feature_name');

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

    public function listings(){
        return $this->belongsToMany('App\Listing', 'feature_listings')->withPivot('uuid')->withTimestamps();
    }

}
