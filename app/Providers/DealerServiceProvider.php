<?php  namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class DealerServiceProvider extends ServiceProvider {

    protected $repositories = [
        'Color', 'Category', 'Condition', 'Feature',  'Fuel', 'Make', 'Transmission','Model','Listing','FeatureListing','VehicleImage','User','Setting'
    ];

    public function register()
    {
        //Loops through all repositories and binds them with their Eloquent implementation
        array_walk($this->repositories, function($repository) {
            $this->app->bind(
                'App\Dealer\Repositories\Contracts\\'. $repository . 'Interface',
                'App\Dealer\Repositories\Eloquent\\' . $repository . 'Repository'
            );
        });

    }



} 