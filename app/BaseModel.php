<?php  namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/*
This class will be used to replace the default 'extends Eloquent'
We need this for the purpose of using a UUID v4 as primary key for every model.
We also override the default save function to try to save the model and regenerate a UUID v4 if it is not unique (which is highly unlikely)
*/
class BaseModel extends Model {

    use softDeletes;

    public $incrementing = false;


    public static function boot() {
        parent::boot();
        static::creating(function($model) {
            // Only generate the uuid if the field actually is called uuid.
            // For some system models a normal id is used (e.g. language)
            if($model->getKeyName() == 'uuid'){
                $model->{$model->getKeyName()} = (string)$model->generateNewId();
            }
        });
    }

    /**
     * Get a new version 4 (random) UUID.
     *
     * @return \Rhumsaa\Uuid\Uuid
     */
    public function generateNewId()
    {
        return \Rhumsaa\Uuid\Uuid::uuid4();
    }

    /**
     * Override save function to catch mySQL error in case of double id's
     * @param array $options
     * @return bool|void
     */
    public function save (array $options = array())
    {
        try{
            parent::save($options);
        }catch(Exception $e){
            // check if the exception is caused by double id
            if(preg_match('/Integrity constraint violation: 1062 Duplicate entry \S+ for key \'PRIMARY\'/', $e->getMessage(), $matches)){
                $this->{$this->getKeyName()} = (string)$this->generateNewId();
                $this->save();
            }
        }
    }

} 