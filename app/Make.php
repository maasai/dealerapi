<?php namespace App;

class Make extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'makes';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array('uuid','make_name');

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

    public function models(){
        return $this->hasMany('App\VehicleModel', 'make_id', 'uuid');
    }

    public function listings(){
        return $this->hasMany('App\Listing', 'listing_id', 'uuid');
    }

}
