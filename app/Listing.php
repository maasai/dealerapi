<?php namespace App;


class Listing extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'listings';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array(
        'uuid','category_id',  'make_id',  'color_id',  'condition_id',  'extra_details',
       'featured',  'fuel_id',  'model_id',  'mileage',  'price',  'registration_year',  'transmission_id',  'active',
        'vin_number','engine_size','doors','exterior_color_id','interior_color_id',
    );

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function interior_color(){
        return $this->belongsTo('App\Color');
    }
    public function exterior_color(){
        return $this->belongsTo('App\Color');
    }


    public function condition(){
        return $this->belongsTo('App\Condition');
    }

    public function make(){
        return $this->belongsTo('App\Make');
    }

    public function model(){
        return $this->belongsTo('App\VehicleModel');
    }

    public function fuel(){
        return $this->belongsTo('App\Fuel');
    }

    public function transmission(){
        return $this->belongsTo('App\Transmission');
    }

    public function features(){
        return $this->belongsToMany('App\Feature','feature_listings')->withPivot('uuid')->withTimestamps();
    }

    public function featureListings(){
        return $this->hasMany('App\FeatureListing');
    }

    public function images(){
        return $this->hasMany('App\VehicleImage');
    }

    public function setFeatures($feature_ids = array()){
        $features = $this->featureListings()->get();
        $featureListing = new FeatureListing();
        if($features){
            $current = array();
            foreach($features as $feature){
                if(!in_array($feature->feature_id, $feature_ids)){
                    $featureListing->find($feature->uuid)->delete();
                }
                else{
                    array_push($current, $feature->feature_id);
                }
            }
        }

        foreach($feature_ids as $selected_feature){
            if(!in_array($selected_feature, $current)){
                $uuid = $featureListing->generateNewId();
                $this->features()->attach($selected_feature, ['uuid' => $uuid]);
            }
        }
    }
}
