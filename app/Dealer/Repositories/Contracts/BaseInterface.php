<?php  namespace App\Dealer\Repositories\Contracts;

/**
 * Interface BaseRepository
 * @package App\Dealer\Repositories\Contracts
 */

interface BaseInterface {

    /**
     * Fetch a collection of records for a given entity
     * @return mixed
     */
    function getAll();

    /**
     * Fetch a collection of records for a given entity
     * @return mixed
     */
    function getAllNoPaginate();

    /**
     * Fetch a single item by its id
     * @param $id
     * @return mixed
     */
    function getById($id);

    /**
     * Fetch softdeleted data
     * @param $timestamp /Since the last check. Fetch all new deletes
     * @return mixed
     */
    function getDeleted($timestamp);

    /**
     * fetch records updates since was last checked
     * @param $timestamp /Time it was last checked
     * @return mixed
     */
    function getUpdated($timestamp);

    /**
     * Create a new record
     * @param array $data
     * @return mixed
     */
    function create(array $data);

    /**
     * Update existing record
     * @param array $data
     * @param $uuid
     * @return mixed
     */
    function update(array $data, $uuid);

    /**
     * Remove record from db
     * @param $id
     * @return mixed
     */
    function delete($id);

    /**
     * get the first record from the db
     * @return mixed
     */
    function first();

    function sortBy($field, $direction);

} 