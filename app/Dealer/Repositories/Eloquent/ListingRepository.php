<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Listing;
use App\Dealer\Repositories\Contracts\ListingInterface;

class ListingRepository extends AbstractRepository implements ListingInterface {

    protected $model, $make, $color, $transmission, $condition, $category, $fuel, $feature;


    /**
     * @param Listing $model
     */
    function __construct(Listing $model, MakeRepository $make, ColorRepository $color, TransmissionRepository $transmission, ConditionRepository $condition, CategoryRepository $category, FuelRepository $fuel, FeatureRepository $feature)
    {
        $this->model = $model;
        $this->make  = $make;
        $this->color = $color;
        $this->transmission = $transmission;
        $this->condition = $condition;
        $this->category = $category;
        $this->fuel = $fuel;
        $this->feature = $feature;
    }

    public function recent_listings(){
        return $this->model->with('images')->orderBy('created_at', 'desc')->paginate(10);
    }
    public function filter($make, $model){
        if($make != '' && $model != '')
            return $this->model->where(['make_id'=>$make, 'model_id'=>$model])->get();
        else if($make != '' && $model == '')
            return $this->model->where(['make_id'=>$make])->get();
        else
            return $this->model->get();
    }

    public function makes_select(){
        $makes = $this->make->sortBy('make_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Make';
        foreach($makes as $make){
            $options[$make->uuid] =  $make->make_name;
        }
        return $options;
    }
    public function color_select(){
        $colors = $this->color->sortBy('color_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Color';
        foreach($colors as $color){
            $options[$color->uuid] =  $color->color_name;
        }
        return $options;
    }
    public function condition_select(){
        $conditions = $this->condition->sortBy('condition_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Condition';
        foreach($conditions as $condition){
            $options[$condition->uuid] =  $condition->condition_name;
        }
        return $options;
    }
    public function category_select(){
        $categories = $this->category->sortBy('category_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Category';
        foreach($categories as $category){
            $options[$category->uuid] =  $category->category_name;
        }
        return $options;
    }
    public function fuel_select(){
        $fuels = $this->fuel->sortBy('fuel_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Fuel';
        foreach($fuels as $fuel){
            $options[$fuel->uuid] =  $fuel->fuel_name;
        }
        return $options;
    }
    public function transmission_select(){
        $transmissions = $this->transmission->sortBy('transmission_name', 'asc')->getAllNoPaginate();
        $options = array();
        $options[''] = 'Select Transmission';
        foreach($transmissions as $transmission){
            $options[$transmission->uuid] =  $transmission->transmission_name;
        }
        return $options;
    }
    public function models_select($make = null){
        $make = $this->make->getById($make, ['models']);
        $options = array();
        $options[''] = 'Select Model';
        foreach($make['models'] as $model){
            $options[$model->uuid] =  $model->model_name;
        }
        return $options;
    }
    public function feature_select(){
        $features = $this->feature->sortBy('feature_name', 'asc')->getAllNoPaginate();
        return $features;
    }
} 