<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Category;
use App\Dealer\Repositories\Contracts\CategoryInterface;

class CategoryRepository extends AbstractRepository implements CategoryInterface {

    protected $model;

    /**
     * @param Category $model
     */
    function __construct(Category $model)
    {
        $this->model = $model;
    }


} 