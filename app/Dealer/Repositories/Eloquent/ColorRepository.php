<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Color;
use App\Dealer\Repositories\Contracts\ColorInterface;

class ColorRepository extends AbstractRepository implements ColorInterface {

    protected $model;

    /**
     * @param Color $model
     */
    function __construct(Color $model)
    {
        $this->model = $model;
    }


} 