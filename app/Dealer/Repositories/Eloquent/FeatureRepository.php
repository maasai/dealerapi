<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Feature;
use App\Dealer\Repositories\Contracts\FeatureInterface;

class FeatureRepository extends AbstractRepository implements FeatureInterface {

    protected $model;

    /**
     * @param Feature $model
     */
    function __construct(Feature $model)
    {
        $this->model = $model;
    }


} 