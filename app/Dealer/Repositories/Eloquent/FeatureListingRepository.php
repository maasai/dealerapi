<?php  namespace App\Dealer\Repositories\Eloquent;


use App\FeatureListing;
use App\Dealer\Repositories\Contracts\FeatureListingInterface;

class FeatureListingRepository extends AbstractRepository implements FeatureListingInterface {

    protected $model;

    /**
     * @param FeatureListing $model
     */
    function __construct(FeatureListing $model)
    {
        $this->model = $model;
    }
} 