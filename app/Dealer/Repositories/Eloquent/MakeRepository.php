<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Make;
use App\Dealer\Repositories\Contracts\MakeInterface;

class MakeRepository extends AbstractRepository implements MakeInterface {

    protected $model;

    /**
     * @param Make $model
     */
    function __construct(Make $model)
    {
        $this->model = $model;
    }

    public function makes_select(){
        $makes = $this->getAll();
        $options = array();
        $options[''] = 'Select Make';
        foreach($makes as $make){
            $options[$make->uuid] =  $make->make_name;
        }
        return $options;
    }


} 