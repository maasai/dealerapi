<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Setting;
use App\Dealer\Repositories\Contracts\SettingInterface;

class SettingRepository extends AbstractRepository implements SettingInterface {

    protected $model;

    /**
     * @param Setting $model
     */
    function __construct(Setting $model)
    {
        $this->model = $model;
    }
} 