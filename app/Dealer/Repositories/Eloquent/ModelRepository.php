<?php  namespace App\Dealer\Repositories\Eloquent;


use App\VehicleModel;
use App\Dealer\Repositories\Contracts\ModelInterface;

class ModelRepository extends AbstractRepository implements ModelInterface {

    protected $model;

    /**
     * @param VehicleModel $model
     */
    function __construct(VehicleModel $model)
    {
        $this->model = $model;
    }

    public function filter($make, $model){
        if($make != '' && $model != '')
            return $this->model->where(['make_id'=>$make])->where('model_name','like','%'.$model.'%')->get();
        else if($make != '' && $model == '')
            return $this->model->where(['make_id'=>$make])->get();
        else
            return $this->model->get();
    }
} 