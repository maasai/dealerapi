<?php  namespace App\Dealer\Repositories\Eloquent;

use App\User;
use App\Dealer\Repositories\Contracts\UserInterface;

class UserRepository extends AbstractRepository implements UserInterface {

    protected $model;

    /**
     * @param User $model
     */
    function __construct(User $model)
    {
        $this->model = $model;
    }
} 