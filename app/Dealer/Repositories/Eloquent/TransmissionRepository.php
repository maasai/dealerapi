<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Transmission;
use App\Dealer\Repositories\Contracts\TransmissionInterface;

class TransmissionRepository extends AbstractRepository implements TransmissionInterface {

    protected $model;

    /**
     * @param Transmission $model
     */
    function __construct(Transmission $model)
    {
        $this->model = $model;
    }

    public function listings(){
        return $this->hasMany('App\Listing');
    }
} 