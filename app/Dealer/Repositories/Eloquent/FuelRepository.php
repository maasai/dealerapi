<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Fuel;
use App\Dealer\Repositories\Contracts\FuelInterface;

class FuelRepository extends AbstractRepository implements FuelInterface {

    protected $model;

    /**
     * @param Fuel $model
     */
    function __construct(Fuel $model)
    {
        $this->model = $model;
    }


} 