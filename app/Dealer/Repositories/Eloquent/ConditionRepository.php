<?php  namespace App\Dealer\Repositories\Eloquent;


use App\Condition;
use App\Dealer\Repositories\Contracts\ConditionInterface;

class ConditionRepository extends AbstractRepository implements ConditionInterface {

    protected $model;

    /**
     * @param Condition $model
     */
    function __construct(Condition $model)
    {
        $this->model = $model;
    }


} 