<?php  namespace App\Dealer\Repositories\Eloquent;


use App\VehicleImage;
use App\Dealer\Repositories\Contracts\VehicleImageInterface;

class VehicleImageRepository extends AbstractRepository implements VehicleImageInterface {

    protected $model;

    /**
     * @param VehicleImage $model
     */
    function __construct(VehicleImage $model)
    {
        $this->model = $model;
    }
} 