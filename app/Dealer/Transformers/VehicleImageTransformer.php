<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class TransmissionTransformer
 * @package App\Dealer\Transformers
 */
class VehicleImageTransformer extends BaseTransformer{

    /**
     * @param $image
     * @return array|mixed
     */
    public function transform($image)
    {
        $allFields = [
            'vehicle_image_id'  => $image['uuid'],
            'filename'          => $image['filename'],
            'listing_id'        => $image['listing_id'],
            'created_at'        => $image['created_at'],
            'updated_at'        => $image['updated_at']
        ];

        return $allFields;
    }

} 