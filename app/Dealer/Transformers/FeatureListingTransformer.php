<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class TransmissionTransformer
 * @package App\Dealer\Transformers
 */
class FeatureListingTransformer extends BaseTransformer{

    /**
     * @param $featurelisting
     * @return array|mixed
     */
    public function transform($featurelisting)
    {
        $allFields = [
            'feature_listing_id'    => $featurelisting['uuid'],
            'feature_id'           => $featurelisting['feature_id'],
            'listing_id'           => $featurelisting['listing_id'],
            'created_at'           => $featurelisting['created_at'],
            'updated_at'           => $featurelisting['updated_at']
        ];

         return $allFields;
    }

} 