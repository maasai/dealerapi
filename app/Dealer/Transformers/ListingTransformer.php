<?php  namespace App\Dealer\Transformers;


/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class TransmissionTransformer
 * @package App\Dealer\Transformers
 */
class ListingTransformer extends BaseTransformer{
      /**
     * @param $listing
     * @return array|mixed
     */
    public function transform($listing)
    {
        $allFields = [
            'listing_id'        => $listing['uuid'],
            'interior_color_id' => $listing['interior_color_id'],
            'exterior_color_id' => $listing['exterior_color_id'],
            'vin_number'        => $listing['vin_number'],
            'engine_size'       => $listing['engine_size'],
            'doors'             => $listing['doors'],
            'vin_number'        => $listing['vin_number'],
            'category_id'       => $listing['category_id'],
            'condition_id'      => $listing['condition_id'],
            'extra_details'     => $listing['extra_details'],
            'featured'          => $listing['featured'],
            'fuel_id'           => $listing['fuel_id'],
            'make_id'           => $listing['make_id'],
            'model_id'          => $listing['model_id'],
            'mileage'           => $listing['mileage'],
            'price'             => $listing['price'],
            'registration_year' => $listing['registration_year'],
            'transmission_id'   => $listing['transmission_id'],
            'active'            => $listing['active'],
            'created_at'        => $listing['created_at'],
            'updated_at'        => $listing['updated_at']
        ];

        return $allFields;
    }

} 