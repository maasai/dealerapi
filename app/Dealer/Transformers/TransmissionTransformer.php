<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class TransmissionTransformer
 * @package App\Dealer\Transformers
 */
class TransmissionTransformer extends BaseTransformer{

    /**
     * @param $transmission
     * @return array|mixed
     */
    public function transform($transmission)
    {
        $allFields = [
            'transmission_id'     => $transmission['uuid'],
            'transmission_name'   => $transmission['transmission_name'],
            'created_at'          => $transmission['created_at'],
            'updated_at'          => $transmission['updated_at']
        ];

        return $allFields;
    }

} 