<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class ColorTransformer
 * @package App\Dealer\Transformers
 */
class ColorTransformer extends BaseTransformer{

    /**
     * @param $color
     * @return array|mixed
     */
    public function transform($color)
    {
        $allFields = [
            'color_id'      => $color['uuid'],
            'color_name'    => $color['color_name'],
            'created_at'    => $color['created_at'],
            'updated_at'    => $color['updated_at']
        ];


        return $allFields;
    }

} 