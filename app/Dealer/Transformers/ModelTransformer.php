<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class TransmissionTransformer
 * @package App\Dealer\Transformers
 */
class ModelTransformer extends BaseTransformer{

     /**
     * @param $model
     * @return array|mixed
     */
    public function transform($model)
    {
        $allFields = [
            'model_id'     => $model['uuid'],
            'model_name'   => $model['model_name'],
            'make_id'      => $model['make_id'],
            'created_at'   => $model['created_at'],
            'updated_at'   => $model['updated_at']
        ];

        return $allFields;
    }

} 