<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class ConditionTransformer
 * @package App\Dealer\Transformers
 */
class ConditionTransformer extends BaseTransformer{

    /**
     * @param $condition
     * @return array|mixed
     */
    public function transform($condition)
    {
        $allFields = [
            'condition_id'      => $condition['uuid'],
            'condition_name'    => $condition['condition_name'],
            'created_at'        => $condition['created_at'],
            'updated_at'        => $condition['updated_at']
        ];

        return $allFields;
    }

} 