<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class CategoryTransformer
 * @package App\Dealer\Transformers
 */
class CategoryTransformer extends BaseTransformer{

    /**
     * @param $category
     * @return array|mixed
     */
    public function transform($category)
    {
        $allFields = [
            'category_id'     => $category['uuid'],
            'category_name'   => $category['category_name'],
            'created_at'      => $category['created_at'],
            'updated_at'      => $category['updated_at']
        ];

        return $allFields;
    }

} 