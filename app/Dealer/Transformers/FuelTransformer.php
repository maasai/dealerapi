<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class FuelTransformer
 * @package App\Dealer\Transformers
 */
class FuelTransformer extends BaseTransformer{

    /**
     * @param $fuel
     * @return array|mixed
     */
    public function transform($fuel)
    {
        $allFields = [
            'fuel_id'     => $fuel['uuid'],
            'fuel_name'   => $fuel['fuel_name'],
            'created_at'  => $fuel['created_at'],
            'updated_at'  => $fuel['updated_at']
        ];

         return $allFields;
    }

} 