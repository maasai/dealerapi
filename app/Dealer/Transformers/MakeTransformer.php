<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class MakeTransformer
 * @package App\Dealer\Transformers
 */
class MakeTransformer extends BaseTransformer{

    /**
     * @param $make
     * @return array|mixed
     */
    public function transform($make)
    {
        $allFields = [
            'make_id'     => $make['uuid'],
            'make_name'   => $make['make_name'],
            'created_at'  => $make['created_at'],
            'updated_at'  => $make['updated_at']
        ];

        return $allFields;
    }

} 