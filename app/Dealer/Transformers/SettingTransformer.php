<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class TransmissionTransformer
 * @package App\Dealer\Transformers
 */
class SettingTransformer extends BaseTransformer{

    /**
     * @param $setting
     * @return array|mixed
     */
    public function transform($setting)
    {
        $allFields = [
            'setting_id'            => $setting['uuid'],
            'company_name'          => $setting['name'],
            'company_description'   => $setting['description'],
            'company_email'         => $setting['email'],
            'company_address'       => $setting['address'],
            'company_city'          => $setting['city'],
            'company_state'         => $setting['state'],
            'company_postal_code'   => $setting['postal_code'],
            'company_telephone'     => $setting['telephone'],
            'company_logo'          => $setting['logo'],
            'company_currency'      => $setting['currency'],
            'company_mileage'       => $setting['mileage_units'],
            'company_longitude'     => $setting['longitude'],
            'company_latitude'      => $setting['latitude'],
            'company_facebook'      => $setting['facebook'],
            'company_twitter'       => $setting['twitter'],
            'company_website'       => $setting['website'],
            'date_format'           => $setting['date_format'],
            'created_at'            => $setting['created_at'],
            'updated_at'            => $setting['updated_at']
        ];
        return $allFields;
    }
} 