<?php  namespace App\Dealer\Transformers;

/**
 * For added security, hide the table fields names where possible by having a layer of field transformations.
 * Class FeatureTransformer
 * @package App\Dealer\Transformers
 */
class FeatureTransformer extends BaseTransformer{

    /**
     * @param $feature
     * @return array|mixed
     */
    public function transform($feature)
    {
        $allFields = [
            'feature_id'    => $feature['uuid'],
            'feature_name'  => $feature['feature_name'],
            'created_at'    => $feature['created_at'],
            'updated_at'    => $feature['updated_at']
        ];

         return $allFields;
    }

} 