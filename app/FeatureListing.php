<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureListing extends BaseModel {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feature_listings';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array('uuid','feature_id',  'listing_id', 'deleted_at');

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

    public function listing(){
        return $this->belongsTo('App\Listing');
    }
    public function feature(){
        return $this->belongsTo('App\Feature');
    }
}
