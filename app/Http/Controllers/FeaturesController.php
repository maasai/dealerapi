<?php namespace App\Http\Controllers;

use App\Http\Requests\FeatureRequest;
use App\Dealer\Repositories\Contracts\FeatureInterface;
use App\Dealer\Transformers\FeatureTransformer;
use Illuminate\Support\Facades\Request;

class FeaturesController extends ApiController {

    /**
     * @var
     */
    protected $featureRepository;

    /**
     * @var
     */

    protected $featureTransformer;

    public function __construct(FeatureInterface $featureRepository, FeatureTransformer $featureTransformer){
        $this->featureRepository = $featureRepository;
        $this->featureTransformer = $featureTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->featureRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->featureRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->featureRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->featureTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No features found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->featureTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param FeatureRequest $request
     * @return mixed
     */
    public function store(FeatureRequest $request)
	{
        if($this->featureRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Feature has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $feature = $this->featureRepository->getById($uuid);

        if(!$feature)
        {
            return $this->respondNotFound('Feature not found.');
        }

        return $this->respond([
            'data' => $this->featureTransformer->transform($feature)
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param FeatureRequest $request
     * @param $uuid
     * @return mixed
     *
     */
    public function update(FeatureRequest $request, $uuid)
	{
        if($this->featureRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! feature has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     *
     */
    public function destroy($uuid)
	{
        if($this->featureRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Feature has been deleted');
        }
        return $this->respondNotFound('Feature not deleted');
	}

}
