<?php namespace App\Http\Controllers;

use App\Color;
use App\Dealer\Repositories\Contracts\ColorInterface;
use App\Dealer\Transformers\ColorTransformer;
use App\Http\Requests\ColorRequest;
use Illuminate\Support\Facades\Request;


class ColorsController extends ApiController {

    /**
     * @var
     */
    protected $colorRepository;

    /**
     * @var
     */
    protected $colorTransformer;

    public function __construct(ColorInterface $colorRepository, ColorTransformer $colorTransformer)
    {
        $this->colorRepository     = $colorRepository;
        $this->colorTransformer    = $colorTransformer;
    }

	/**
	 * Display a listing of the resource. Will sync with the caller via timestamps
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);

        $syncData = $this->colorRepository->getAll();

        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->colorRepository->getUpdated($modifiedSince);
            //send a list of ids to desync
            $desync = $this->colorRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
               'data' => $this->colorTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

       //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
           return $this->respondNotFound('No colors found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->colorTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param ColorRequest $request
     * @return mixed
     */
    public function store(ColorRequest $request)
	{
        if($this->colorRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Color has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");

    }

	/**
	 * Display the specified resource.
	 *
	 * @param  $uuid
	 * @return Response
	 */
	public function show($uuid)
	{
        $color = $this->colorRepository->getById($uuid);

        if(!$color)
        {
            return $this->respondNotFound('Color not found.');
        }

        return $this->respond([
            'data' => $this->colorTransformer->transform($color)
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $uuid
	 * @return Response
	 */
	public function edit($uuid)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param ColorRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ColorRequest $request, $uuid)
	{
        if($this->colorRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Color has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $uuid
	 * @return Response
	 */
	public function destroy($uuid)
	{
		if($this->colorRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Color has been deleted');
        }
        return $this->respondNotFound('Color not deleted');

    }

}
