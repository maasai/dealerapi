<?php namespace App\Http\Controllers;

use App\Http\Requests\ConditionRequest;
use App\Dealer\Repositories\Contracts\ConditionInterface;
use App\Dealer\Transformers\ConditionTransformer;
use Illuminate\Support\Facades\Request;

class ConditionsController extends ApiController {

    /**
     * @var
     */
    protected $conditionRepository;

    /**
     * @var
     */
    protected $conditionTransformer;

    public function __construct(ConditionInterface $conditionRepository, ConditionTransformer $conditionTransformer){
        $this->conditionRepository = $conditionRepository;
        $this->conditionTransformer = $conditionTransformer;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->conditionRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->conditionRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->conditionRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->conditionTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No conditions found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->conditionTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


    /**
     *  Store a newly created resource in storage.
     * @param ConditionRequest $request
     * @return mixed
     */
    public function store(ConditionRequest $request)
	{
        if($this->conditionRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Condition has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

	/**
     *Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $condition = $this->conditionRepository->getById($uuid);

        if(!$condition)
        {
            return $this->respondNotFound('Condition not found.');
        }

        return $this->respond([
            'data' => $this->conditionTransformer->transform($condition)
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param ConditionRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ConditionRequest $request, $uuid)
	{
        if($this->conditionRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! condition has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->conditionRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! condition has been deleted');
        }
        return $this->respondNotFound('Condition not deleted');
	}

}
