<?php namespace App\Http\Controllers;

use App\Http\Requests\MakeRequest;
use App\Dealer\Repositories\Contracts\MakeInterface;
use App\Dealer\Transformers\MakeTransformer;
use Illuminate\Support\Facades\Request;

class MakesController extends ApiController {

    /**
     * @var
     */
    protected $makeRepository;

    /**
     * @var
     */

    protected $makeTransformer;

    public function __construct(MakeInterface $makeRepository, MakeTransformer $makeTransformer){
        $this->makeRepository = $makeRepository;
        $this->makeTransformer = $makeTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->makeRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->makeRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->makeRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->makeTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No vehicle makes found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->makeTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param MakeRequest $request
     * @return mixed
     */
    public function store(MakeRequest $request)
    {
        if($this->makeRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Vehicle make has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $make = $this->makeRepository->getById($uuid);

        if(!$make)
        {
            return $this->respondNotFound('Make not found.');
        }

        return $this->respond([
            'data' => $this->makeTransformer->transform($make)
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param MakeRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(MakeRequest $request, $uuid)
    {
        if($this->makeRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Vehicle make has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->makeRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Vehicle make has been deleted');
        }
        return $this->respondNotFound('Vehicle make not deleted');
    }


}
