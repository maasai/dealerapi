<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\FeatureRequest;
use App\Dealer\Repositories\Contracts\FeatureInterface;
use Illuminate\Support\Facades\Request;

class FeaturesController extends AdminController {

    /**
     * @var
     */
    protected $featureRepository;

    public function __construct(FeatureInterface $featureRepository){
        $this->featureRepository = $featureRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $features = $this->featureRepository->sortBy('feature_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($features,[
            'features' => $features->all()
        ]);
        if(Request::ajax()){
            $view = view('features.partials._features', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('features.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('features.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param FeatureRequest $request
     * @return mixed
     */
    public function store(FeatureRequest $request)
	{
        if($this->featureRepository->create($request->all())){
            \Session::flash('success', 'Success !! Feature has been added.');
            return $this->respondWithSuccess('Success !! Feature has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $feature = $this->featureRepository->getById($uuid);

        if(!$feature)
        {
            return $this->respondNotFound('Feature not found.');
        }

        return $this->respond([
            'data' => $this->featureTransformer->transform($feature)
        ]);
	}
    /**
     * Update the specified resource in storage.
     * @param FeatureRequest $request
     * @param $uuid
     * @return mixed
     *
     */
    public function update(FeatureRequest $request, $uuid)
	{
        if($this->featureRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! feature has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     *
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $feature){
                $this->featureRepository->delete($feature);
            }
            \Session::flash('success', 'Success !! Features have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Features deleted successfully'], 200);
        }
        else{
            if($this->featureRepository->delete($uuid)){
                \Session::flash('success', 'Success !!Feature has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting feature try again.');
            }
            return redirect('backend/features');
	    }
    }

}
