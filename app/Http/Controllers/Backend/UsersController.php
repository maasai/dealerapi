<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\UserRequest;
use App\Http\Requests\ProfileRequest;
use App\Dealer\Repositories\Contracts\UserInterface;
use Illuminate\Support\Facades\Request;

class UsersController extends AdminController {

    /**
     * @var
     */
    protected $userRepository;


    public function __construct(UserInterface $userRepository){
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->userRepository->sortBy('name', 'asc')->getAll();
        $data =  $this->respondWithPagination($users,[
            'users' => $users->all()
        ]);
        if(Request::ajax()){
            $view = view('users.partials._users', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('users.index',compact('data'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('users.create');
	}

    /**
     * Store a newly created resource in storage.
     * @param UserRequest $request
     * @return mixed
     */
    public function store(UserRequest $request)
    {
        $data = array('username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password)
        );
        if($this->userRepository->create($data)){
            \Session::flash('success', 'Success !! User has been created.');
            return $this->respondWithSuccess('Success !! User has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Show the form for editing the specified resource.
     * @param $uuid
     * @return \Illuminate\View\View
     */
    public function edit($uuid)
    {
        $user = $this->userRepository->getById($uuid);
        return view('users.edit', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     * @param UserRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(UserRequest $request, $uuid)
    {
        $data = array('username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
        );
        if($request->password != ''){
            $data['password'] = bcrypt($request->password);
        }
        if($this->userRepository->update($data, $uuid)){
            return $this->respondWithSuccess('Success !! User has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * @return \Illuminate\View\View
     */
    public function editProfile(){
        $user = $this->userRepository->getById(\Auth::user()->uuid);
        return view('users.profile', compact('user'));
    }

    /**
     * @param ProfileRequest $request
     * @return mixed
     */
    public function updateProfile(ProfileRequest $request){
        $user = $this->userRepository->getById(\Auth::user()->uuid);
        $data = array(
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
        );
        if ($request->hasFile('photo'))
        {
            $file = $request->file('photo');
            $filename = strtolower(str_random(50) . '.' . $file->getClientOriginalExtension());
            $file->move('assets/img/users', $filename);
            \Image::make(sprintf('assets/img/users/%s', $filename))->resize(200, 200)->save();
            \File::delete('assets/img/users/'.$user->photo);
            $data['photo']= $filename;
        }

        if($request->get('password') != ''){
            $data['password']= bcrypt($request->password);
        }
        $this->userRepository->update($data, $user->uuid);

        if($this->userRepository->update($data, $user->uuid)){
            \Session::flash('success', 'Success!! Profile has been updated.');
            return $this->respondWithSuccess('Success !! User has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->userRepository->delete($uuid)){
            \Session::flash('success', 'Success !! User has been deleted.');
        }else{
            \Session::flash('error', 'Error !! Error deleting User try again.');
        }
        return redirect('backend/users');
    }


}
