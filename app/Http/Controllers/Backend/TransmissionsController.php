<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\TransmissionRequest;
use App\Dealer\Repositories\Contracts\TransmissionInterface;
use Illuminate\Support\Facades\Request;

class TransmissionsController extends AdminController {

    /**
     * @var
     */
    protected $transmissionRepository;


    public function __construct(TransmissionInterface $transmissionRepository){
        $this->transmissionRepository = $transmissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $transmissions = $this->transmissionRepository->sortBy('transmission_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($transmissions,[
            'transmissions' => $transmissions->all()
        ]);
        if(Request::ajax()){
            $view = view('transmissions.partials._transmissions', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('transmissions.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('transmissions.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param TransmissionRequest $request
     * @return mixed
     */
    public function store(TransmissionRequest $request)
    {
        if($this->transmissionRepository->create($request->all())){
            \Session::flash('success', 'Success !! Vehicle transmission has been created.');
            return $this->respondWithSuccess('Success !! Vehicle transmission has been created.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $transmission = $this->transmissionRepository->getById($uuid);

        if(!$transmission)
        {
            return $this->respondNotFound('Transmission not found.');
        }

        return $this->respond([
            'data' => $this->transmissionTransformer->transform($transmission)
        ]);

    }

    /**
     * Update the specified resource in storage.
     * @param TransmissionRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(TransmissionRequest $request, $uuid)
    {
        if($this->transmissionRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Vehicle transmission has been updated.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $transmission){
                $this->transmissionRepository->delete($transmission);
            }
            \Session::flash('success', 'Success !! Vehicle transmissions have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Vehicle transmissions deleted successfully'], 200);
        }
        else{
            if($this->transmissionRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Vehicle transmission has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting Vehicle transmission try again.');
            }
            return redirect('backend/transmissions');
        }
    }
}
