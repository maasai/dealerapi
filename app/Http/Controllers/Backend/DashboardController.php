<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Dealer\Repositories\Contracts\ListingInterface;
use App\Dealer\Repositories\Contracts\MakeInterface;
use App\Dealer\Repositories\Contracts\ModelInterface;
use App\Dealer\Repositories\Contracts\CategoryInterface;

class DashboardController extends AdminController {
    /**
     * @var
     */
    protected $listingRepository;
    /**
     * @var
     */
    protected $makeRepository;
    /**
     * @var
     */
    protected $modelRepository;
    /**
     * @var
     */
    protected $categoryRepository;

    public function __construct(ListingInterface $listingRepository, MakeInterface $makeRepository,  ModelInterface $modelRepository,  CategoryInterface $categoryRepository){
        $this->listingRepository = $listingRepository;
        $this->makeRepository = $makeRepository;
        $this->modelRepository = $modelRepository;
        $this->categoryRepository = $categoryRepository;
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        /* stats fetch */
        $stats['listings']   = $this->listingRepository->count();
        $stats['makes']      = $this->makeRepository->count();;
        $stats['models']     = $this->modelRepository->count();
        $stats['categories'] = $this->categoryRepository->count();

        $listings = $this->listingRepository->recent_listings();
        $data =  $this->respondWithPagination($listings,[
            'listings' => $listings->all()
        ]);

		return view('dashboard.index',compact('data', 'stats'));
	}
}
