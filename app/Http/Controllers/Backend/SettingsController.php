<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\SettingFormRequest;
use App\Dealer\Repositories\Contracts\SettingInterface;

class SettingsController extends AdminController {

    private $settingRepository;

    /**
     * @param SettingInterface $settingRepository
     */

    public function __construct(SettingInterface $settingRepository){
        $this->settingRepository = $settingRepository;

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $settings = $this->settingRepository->first();
        return view('settings.index',compact('settings'));
	}

    /**
     * Store a newly created resource in storage.
     * @param SettingFormRequest $request
     * @return mixed
     */
    public function store(SettingFormRequest $request)
	{
		$settings = [
            'name'      => $request->get('name'),
            'description' => $request->get('description'),
            'email'     => $request->get('email'),
            'address'   => $request->get('address'),
            'city'      => $request->get('city'),
            'state'     => $request->get('state'),
            'postal_code' => $request->get('postal_code'),
            'telephone' => $request->get('telephone'),
            'mileage_units' => $request->get('mileage_units'),
            'currency'  => $request->get('currency'),
            'longitude' => $request->get('longitude'),
            'latitude'  => $request->get('latitude'),
            'facebook'  => $request->get('facebook'),
            'twitter'   => $request->get('twitter'),
            'website'   => $request->get('website'),
            'date_format' => $request->get('date_format')
        ];

        if ($request->hasFile('logo'))
        {
            $file = $request->file('logo');
            $filename = strtolower(str_random(50) . '.' . $file->getClientOriginalExtension());
            $file->move('assets/img', $filename);
            \Image::make(sprintf('assets/img/%s', $filename))->resize(200,200, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            $settings['logo']= $filename;
        }

        if($this->settingRepository->create($settings)){
            \Session::flash('success', 'Success !! Settings has been created.');
            return $this->respondWithSuccess('Success !! Settings has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened, Settings not saved");
	}

    /**
     * Update the specified resource in storage.
     * @param SettingFormRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(SettingFormRequest $request, $uuid)
	{
        $setting = $this->settingRepository->getById($uuid);
        $settings = [
            'name'      => $request->get('name'),
            'description'=> $request->get('description'),
            'email'     => $request->get('email'),
            'address'   => $request->get('address'),
            'city'      => $request->get('city'),
            'state'     => $request->get('state'),
            'postal_code' => $request->get('postal_code'),
            'telephone' => $request->get('telephone'),
            'mileage_units' => $request->get('mileage_units'),
            'currency'  => $request->get('currency'),
            'longitude' => $request->get('longitude'),
            'latitude'  => $request->get('latitude'),
            'facebook'  => $request->get('facebook'),
            'twitter'   => $request->get('twitter'),
            'website'   => $request->get('website'),
            'date_format' => $request->get('date_format')
        ];

        if ($request->hasFile('logo'))
        {
            $file = $request->file('logo');
            $filename = strtolower(str_random(50) . '.' . $file->getClientOriginalExtension());
            $file->move('assets/img', $filename);
            \Image::make(sprintf('assets/img/%s', $filename))->resize(200,200, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            $settings['logo']= $filename;
            if(is_file('assets/img/'.$setting->logo)){
                unlink('assets/img/'.$setting->logo);
            }
        }

        if($this->settingRepository->update($settings, $uuid)){
            \Session::flash('success', 'Success !! Settings has been updated.');
            return $this->respondWithSuccess('Success !! Settings has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened, Settings not saved");


	}
}
