<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\MakeRequest;
use App\Dealer\Repositories\Contracts\MakeInterface;
use Illuminate\Support\Facades\Request;

class MakesController extends AdminController {

    /**
     * @var
     */
    protected $makeRepository;


    public function __construct(MakeInterface $makeRepository){
        $this->makeRepository = $makeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $makes = $this->makeRepository->sortBy('make_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($makes,[
            'makes' => $makes->all()
        ]);
        if(Request::ajax()){
            $view = view('makes.partials._makes', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('makes.index',compact('data'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('makes.create');
	}

    /**
     * Store a newly created resource in storage.
     * @param MakeRequest $request
     * @return mixed
     */
    public function store(MakeRequest $request)
    {
        if($this->makeRepository->create($request->all())){
            \Session::flash('success', 'Success !! Vehicle make has been created.');
            return $this->respondWithSuccess('Success !! Vehicle make has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $make = $this->makeRepository->getById($uuid);

        if(!$make)
        {
            return $this->respondNotFound('Make not found.');
        }

        return $this->respond([
            'data' => $this->makeTransformer->transform($make)
        ]);

    }
    /**
     * Update the specified resource in storage.
     * @param MakeRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(MakeRequest $request, $uuid)
    {
        if($this->makeRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Vehicle make has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $make){
                $this->makeRepository->delete($make);
            }
            \Session::flash('success', 'Success !! Vehicle makes have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Vehicle makes deleted successfully'], 200);
        }
        else{
            if($this->makeRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Vehicle make has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting Vehicle make try again.');
            }
            return redirect('backend/makes');
        }
    }


}
