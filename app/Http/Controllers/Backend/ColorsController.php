<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\ColorRequest;
use App\Dealer\Repositories\Contracts\ColorInterface;
use Illuminate\Support\Facades\Request;

class ColorsController extends AdminController {
    /**
     * @var
     */
    protected $colorRepository;


    public function __construct(ColorInterface $colorRepository)
    {
        $this->colorRepository     = $colorRepository;
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $colors = $this->colorRepository->sortBy('color_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($colors,[
            'colors' => $colors->all()
        ]);
        if(Request::ajax()){
            $view = view('colors.partials._colors', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
		return view('colors.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('colors.create');
	}

    /**
     * Store a newly created resource in storage.
     * @param ColorRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ColorRequest $request)
	{
        if($this->colorRepository->create($request->all())){
            \Session::flash('success', 'Success !! Color has been added.');
            return $this->respondWithSuccess('Success !! Color has been created.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
	}



    /**
     * Update the specified resource in storage.
     * @param ColorRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ColorRequest $request, $uuid)
	{
        if($this->colorRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Color has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $color){
                $this->colorRepository->delete($color);
            }
            \Session::flash('success', 'Success !! Colors have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'colors deleted successfully'], 200);
        }
        else{
            if($this->colorRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Color has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting color try again.');
            }
            return redirect('backend/colors');
        }
	}

}
