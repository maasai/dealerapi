<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\ConditionRequest;
use App\Dealer\Repositories\Contracts\ConditionInterface;
use Illuminate\Support\Facades\Request;

class ConditionsController extends AdminController {

    /**
     * @var
     */
    protected $conditionRepository;


    public function __construct(ConditionInterface $conditionRepository){
        $this->conditionRepository = $conditionRepository;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $conditions = $this->conditionRepository->sortBy('condition_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($conditions,[
            'conditions' => $conditions->all()
        ]);
        if(Request::ajax()){
            $view = view('colors.partials._colors', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('conditions.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('conditions.create');
	}


    /**
     *  Store a newly created resource in storage.
     * @param ConditionRequest $request
     * @return mixed
     */
    public function store(ConditionRequest $request)
	{
        if($this->conditionRepository->create($request->all())){
            \Session::flash('success', 'Success !! Condition has been added.');
            return $this->respondWithSuccess('Success !! Condition has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

	/**
     *Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $condition = $this->conditionRepository->getById($uuid);

        if(!$condition)
        {
            return $this->respondNotFound('Condition not found.');
        }

        return $this->respond([
            'data' => $this->conditionTransformer->transform($condition)
        ]);
	}

    /**
     * Update the specified resource in storage.
     * @param ConditionRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ConditionRequest $request, $uuid)
	{
        if($this->conditionRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! condition has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $condition){
                $this->conditionRepository->delete($condition);
            }
            \Session::flash('success', 'Success !! Conditions have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Conditions deleted successfully'], 200);
        }
        else{
            if($this->conditionRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Condition has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting condition try again.');
            }
            return redirect('backend/conditions');
	    }
    }

}
