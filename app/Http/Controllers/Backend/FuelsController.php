<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\FuelRequest;
use App\Dealer\Repositories\Contracts\FuelInterface;
use Illuminate\Support\Facades\Request;

class FuelsController extends AdminController {

    /**
     * @var
     */
    protected $fuelRepository;

    public function __construct(FuelInterface $fuelRepository){
        $this->fuelRepository = $fuelRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $fuels = $this->fuelRepository->sortBy('fuel_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($fuels,[
            'fuels' => $fuels->all()
        ]);
        if(Request::ajax()){
            $view = view('fuels.partials._fuels', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('fuels.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('fuels.create');
	}

    /**
     * Store a newly created resource in storage.
     * @param FuelRequest $request
     * @return mixed
     */
    public function store(FuelRequest $request)
	{
        if($this->fuelRepository->create($request->all())){
            \Session::flash('success', 'Success !! Fuel has been added.');
            return $this->respondWithSuccess('Success !! Fuel has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $fuel = $this->fuelRepository->getById($uuid);

        if(!$fuel)
        {
            return $this->respondNotFound('Fuel not found.');
        }

        return $this->respond([
            'data' => $this->fuelTransformer->transform($fuel)
        ]);

	}

    /**
     * Update the specified resource in storage.
     * @param FuelRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(FuelRequest $request, $uuid)
	{
        if($this->fuelRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Fuel has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $fuel){
                $this->fuelRepository->delete($fuel);
            }
            \Session::flash('success', 'Success !! Fuels have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Fuels deleted successfully'], 200);
        }
        else{
            if($this->fuelRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Fuel has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting Fuel try again.');
            }
            return redirect('backend/fuels');
            }
    }

}
