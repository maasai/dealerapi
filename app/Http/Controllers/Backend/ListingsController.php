<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\ListingRequest;
use App\Dealer\Repositories\Contracts\ListingInterface;
use App\Dealer\Repositories\Contracts\VehicleImageInterface;
use App\Dealer\Repositories\Contracts\FeatureListingInterface;
use Illuminate\Support\Facades\Request;

class ListingsController extends AdminController {


    /**
     * @var
     */
    protected $listingRepository;

    /**
     * @var
     */

    protected $listingTransformer;
    /**
     * @var
     */

    protected $listingImagesRepository;

    /**
     * @var
     */

    protected $featureListingRepository;


    public function __construct(ListingInterface $listingRepository, VehicleImageInterface $listingImageRepository, FeatureListingInterface $featureListingRepository){
        $this->listingRepository = $listingRepository;
        $this->listingImagesRepository = $listingImageRepository;
        $this->featureListingRepository = $featureListingRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
       $makes      = $this->listingRepository->makes_select();
       $load = ['images'];
       $listings = $this->listingRepository->getAll($load);
       $data =  $this->respondWithPagination($listings,[
            'listings' => $listings->all()
        ]);
       if(Request::ajax()){
           $view = view('listings.partials._listings', compact('data'))->render();
           $paginator = str_replace('/?', '?', $data['paginator']['links']);
           return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
       }
        return view('listings.index',compact('data', 'makes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $makes      = $this->listingRepository->makes_select();
        $colors     = $this->listingRepository->color_select();
        $conditions = $this->listingRepository->condition_select();
        $categories = $this->listingRepository->category_select();
        $transmissions = $this->listingRepository->transmission_select();
        $fuels      = $this->listingRepository->fuel_select();
        $features      = $this->listingRepository->feature_select();
		return view('listings.create', compact('makes','colors','categories','conditions','transmissions','fuels','features'));
	}

    /**
     * Store a newly created resource in storage.
     * @param ListingRequest $request
     * @return mixed
     */
    public function store(ListingRequest $request)
	{

        $listing = $this->listingRepository->create($request->all());
        if($listing){
            //Images upload
            for($imageCount = 1; $imageCount <= 5; $imageCount++){
                $image_name = 'image_'.$imageCount;
                if ($request->hasFile($image_name))
                {
                    $file = $request->file($image_name);
                    $filename = strtolower(str_random(50) . '.' . $file->getClientOriginalExtension());
                    $file->move('assets/img/vehicle', $filename);
                    \Image::make(sprintf('assets/img/vehicle/%s', $filename))->resize(150,150, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save('assets/img/vehicle/thumb/'.$filename);
                    $imageData = array(
                        'listing_id'  => $listing->uuid,
                        'filename'    => $filename,
                    );
                    $this->listingImagesRepository->create($imageData);
                }
            }
            //save Features
            $features = $request->get('features') ? $request->get('features') : null;
            $listing->setFeatures($features);

            \Session::flash('success', 'Success !! Vehicle listing has been created.');
            return $this->respondWithSuccess('Success !! Vehicle listing has been created.');
        }


        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return \Illuminate\View\View|mixed
     */
    public function show($uuid)
	{
        $listing = $this->listingRepository->getById($uuid, ['featureListings']);
      //  dd($listing);

        if(!$listing)
        {
            return $this->respondNotFound('Listing not found.');
        }

        return view('listings.show', compact('listing'));
	}

    /**
     * Show the form for editing the specified resource.
     * @param $uuid
     * @return \Illuminate\View\View
     */
    public function edit($uuid)
	{
        $makes              = $this->listingRepository->makes_select();
        $colors             = $this->listingRepository->color_select();
        $conditions         = $this->listingRepository->condition_select();
        $categories         = $this->listingRepository->category_select();
        $transmissions      = $this->listingRepository->transmission_select();
        $fuels              = $this->listingRepository->fuel_select();
        $features_select     = $this->listingRepository->feature_select();
        $listing            = $this->listingRepository->getById($uuid, ['featureListings']);
        return view('listings.edit', compact('makes','colors','categories','conditions','transmissions','fuels','features_select', 'listing'));
	}

    /**
     * Update the specified resource in storage.
     * @param ListingRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ListingRequest $request, $uuid)
	{
        if(!$request->has('active')){
            $request->merge(array('active' => 0));
        }
        if(!$request->has('featured')){
            $request->merge(array('featured' => 0));
        }


        $listing = $this->listingRepository->update($request->all(), $uuid);

        if($listing){
            //Images upload
            for($imageCount = 1; $imageCount <= 5; $imageCount++){
                $image_name = 'image_'.$imageCount;
                if ($request->hasFile($image_name))
                {
                    $file = $request->file($image_name);
                    $filename = strtolower(str_random(50) . '.' . $file->getClientOriginalExtension());
                    $file->move('assets/img/vehicle', $filename);
                    \Image::make(sprintf('assets/img/vehicle/%s', $filename))->resize(150,150, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save('assets/img/vehicle/thumb/'.$filename);
                    \Image::make(sprintf('assets/img/vehicle/%s', $filename))->resize(400,400, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save('assets/img/vehicle/mobile/'.$filename);
                    $imageData = array(
                        'listing_id'  => $listing->uuid,
                        'filename'    => $filename,
                    );
                    $this->listingImagesRepository->create($imageData);
                }
            }
            //save Features
            $features = $request->get('features') ? $request->get('features') : array();

            $listing->setFeatures($features);

            \Session::flash('success', 'Success !! Vehicle listing has been updated.');
            return $this->respondWithSuccess('Success !! Vehicle listing has been created.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
	}

    public function getModels(){
        $make = \Request::get('make');
        $models = $this->listingRepository->models_select($make);
       return $this->respond(['models' => $models]);

    }

    public function filter(){
        $make = Request::get('make');
        $model = Request::get('model');

        $data['listings'] = $this->listingRepository->filter($make, $model);

        $view = view('listings.partials._listings_filter', compact('data'))->render();
        return \Response::json(['success'=> true, 'html' => $view], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $listing_id){
                $listing = $this->listingRepository->getById($listing_id);
                $listing->images()->delete();
                $listing->featureListings()->delete();
                $this->listingRepository->delete($listing_id);
            }
            \Session::flash('success', 'Success !! Vehicle listings have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Vehicle listings deleted successfully'], 200);
        }
        else{
            $listing = $this->listingRepository->getById($uuid);
            $listing->images()->delete();
            $listing->featureListings()->delete();
            if($listing->delete()){
                \Session::flash('success', 'Success !! Vehicle listing has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting Vehicle listing try again.');
            }
            return redirect('backend/listings');
	    }
    }

}
