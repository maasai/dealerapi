<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\VehicleModelRequest;
use App\Dealer\Repositories\Contracts\ModelInterface;
use App\Dealer\Repositories\Contracts\MakeInterface;
use Illuminate\Support\Facades\Request;

class VehicleModelsController extends AdminController {

    /**
     * @var
     */
    protected $modelRepository;

    /**
     * @var
     */
    protected $makeRepository;


    public function __construct(ModelInterface $modelRepository, MakeInterface $makeRepository){
        $this->modelRepository = $modelRepository;
        $this->makeRepository = $makeRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $makes = $this->makeRepository->makes_select();
        $models = $this->modelRepository->getAll(['make']);
        $data =  $this->respondWithPagination($models,[
            'models' => $models->all()
        ]);
        if(Request::ajax()){
            $view = view('models.partials._models', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('models.index',compact('data', 'makes'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $makes = $this->makeRepository->makes_select();
        return view('models.create', compact('makes'));
	}

    /**
     * Store a newly created resource in storage.
     * @param VehicleModelRequest $request
     * @return mixed
     */
    public function store(VehicleModelRequest $request)
	{
        if($this->modelRepository->create($request->all())){
            \Session::flash('success', 'Success !! Vehicle model has been created.');
            return $this->respondWithSuccess('Success !! Vehicle model has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $model = $this->modelRepository->getById($uuid);

        if(!$model)
        {
            return $this->respondNotFound('Model not found.');
        }

        return $this->respond([
            'data' => $this->modelTransformer->transform($model)
        ]);
	}

    /**
     * Show the form for editing the specified resource.
     * @param $uuid
     * @return \Illuminate\View\View
     */
    public function edit($uuid)
	{
        $model = $this->modelRepository->getById($uuid, ['make']);
        $makes = $this->makeRepository->makes_select();
        return view('models.edit', compact('model', 'makes'));
	}

    /**
     * Update the specified resource in storage.
     * @param VehicleModelRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(VehicleModelRequest $request, $uuid)
	{
        if($this->modelRepository->update($request->all(), $uuid)){
            \Session::flash('success', 'Success !! Vehicle model has been updated.');
            return $this->respondWithSuccess('Success !! Vehicle model has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * @return mixed
     */

    public function filter(){
        $make = Request::get('make');
        $model = Request::get('model');

        $data['models'] = $this->modelRepository->filter($make, $model);

        $view = view('models.partials._models_filter', compact('data'))->render();
        return \Response::json(['success'=> true, 'html' => $view], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $model){
                $this->modelRepository->delete($model);
            }
            \Session::flash('success', 'Success !! Vehicle models have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Vehicle models deleted successfully'], 200);
        }
        else{
            if($this->modelRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Vehicle model has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting Vehicle model try again.');
            }
            return redirect('backend/models');
	    }
    }

}
