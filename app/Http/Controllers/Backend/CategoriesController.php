<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests\CategoryRequest;
use App\Dealer\Repositories\Contracts\CategoryInterface;
use Illuminate\Support\Facades\Request;

class CategoriesController extends AdminController {

    /*
     * Repository variable
     */

    protected $categoryRepository;


    public function __construct(CategoryInterface $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $categories = $this->categoryRepository->sortBy('category_name', 'asc')->getAll();
        $data =  $this->respondWithPagination($categories,[
            'categories' => $categories->all()
        ]);
        if(Request::ajax()){
            $view = view('categories.partials._categories', compact('data'))->render();
            $paginator = str_replace('/?', '?', $data['paginator']['links']);
            return \Response::json(['success'=> true, 'html' => $view, 'paginator'=>$paginator], 200);
        }
        return view('categories.index',compact('data'));
}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('categories.create');
	}

    /**
     * Store a newly created resource in storage.
     * @param CategoryRequest $request
     * @return mixed
     */
    public function store(CategoryRequest $request)
	{
        if($this->categoryRepository->create($request->all())){
            \Session::flash('success', 'Success !! category has been added.');
            return $this->respondWithSuccess('Success !! category has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}


    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $category = $this->categoryRepository->getById($uuid);

        if(!$category)
        {
            return $this->respondNotFound('Category not found.');
        }

        return $this->respond([
            'data' => $this->categoryTransformer->transform($category)
        ]);
	}


    /**
     * Update the specified resource in storage.
     * @param CategoryRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(CategoryRequest $request, $uuid)
	{
        if($this->categoryRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Category has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($uuid == 'delete_multiple'){
            $items = json_decode($_POST['items']);
            foreach($items as $category){
                $this->categoryRepository->delete($category);
            }
            \Session::flash('success', 'Success !! Categories have been deleted.');
            return \Response::json(['success'=> true, 'message' => 'Categories deleted successfully'], 200);
        }
        else{
            if($this->categoryRepository->delete($uuid)){
                \Session::flash('success', 'Success !! Category has been deleted.');
            }else{
                \Session::flash('error', 'Error !! Error deleting Category try again.');
            }
            return redirect('backend/categories');
        }
	}

}
