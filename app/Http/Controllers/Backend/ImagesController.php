<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Dealer\Repositories\Contracts\VehicleImageInterface;

class ImagesController extends AdminController {

    protected $imageRepository;

    public function __construct(VehicleImageInterface $imageRepository){
        $this->imageRepository = $imageRepository;
    }
    /**
     *
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->imageRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Image has been deleted');
        }
        return $this->respondNotFound('Image not deleted');
	}

}
