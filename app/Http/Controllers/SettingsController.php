<?php namespace App\Http\Controllers;

use App\Dealer\Repositories\Contracts\SettingInterface;
use App\Dealer\Transformers\SettingTransformer;
use Illuminate\Support\Facades\Request;

class SettingsController extends ApiController {

    private $settingRepository, $settingTransformer;

    /**
     * @param SettingInterface $settingRepository
     * @param SettingTransformer $settingTransformer
     */

    public function __construct(SettingInterface $settingRepository, SettingTransformer $settingTransformer){
        $this->settingRepository = $settingRepository;
        $this->settingTransformer = $settingTransformer;

    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->settingRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->settingRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->settingRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->settingTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No categories found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->settingTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}
}
