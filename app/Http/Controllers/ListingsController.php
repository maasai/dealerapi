<?php namespace App\Http\Controllers;

use App\Http\Requests\ListingRequest;
use App\Dealer\Repositories\Contracts\ListingInterface;
use App\Dealer\Transformers\ListingTransformer;
use Illuminate\Support\Facades\Request;

class ListingsController extends ApiController {


    /**
     * @var
     */
    protected $listingRepository;

    /**
     * @var
     */

    protected $listingTransformer;


    public function __construct(ListingInterface $listingRepository, ListingTransformer $listingTransformer){
        $this->listingRepository = $listingRepository;
        $this->listingTransformer = $listingTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->listingRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->listingRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->listingRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->listingTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No vehicle listings found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->listingTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param ListingRequest $request
     * @return mixed
     */
    public function store(ListingRequest $request)
	{
        if($this->listingRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Vehicle listing has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $listing = $this->listingRepository->getById($uuid);

        if(!$listing)
        {
            return $this->respondNotFound('Listing not found.');
        }

        return $this->respond([
            'data' => $this->listingTransformer->transform($listing)
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param ListingRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ListingRequest $request, $uuid)
	{
        if($this->listingRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Vehicle listing has been updated.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->listingRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Vehicle listing has been deleted');
        }
        return $this->respondNotFound('Vehicle listing not deleted');
	}

}
