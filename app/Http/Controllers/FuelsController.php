<?php namespace App\Http\Controllers;

use App\Http\Requests\FuelRequest;
use App\Dealer\Repositories\Contracts\FuelInterface;
use App\Dealer\Transformers\FuelTransformer;
use Illuminate\Support\Facades\Request;

class FuelsController extends ApiController {

    /**
     * @var
     */
    protected $fuelRepository;

    /**
     * @var
     */

    protected $fuelTransformer;

    public function __construct(FuelInterface $fuelRepository, FuelTransformer $fuelTransformer){
        $this->fuelRepository = $fuelRepository;
        $this->fuelTransformer = $fuelTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->fuelRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->fuelRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->fuelRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->fuelTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No fuels found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->fuelTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param FuelRequest $request
     * @return mixed
     */
    public function store(FuelRequest $request)
	{
        if($this->fuelRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Fuel has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $fuel = $this->fuelRepository->getById($uuid);

        if(!$fuel)
        {
            return $this->respondNotFound('Fuel not found.');
        }

        return $this->respond([
            'data' => $this->fuelTransformer->transform($fuel)
        ]);

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param FuelRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(FuelRequest $request, $uuid)
	{
        if($this->fuelRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Fuel has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->fuelRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Fuel has been deleted');
        }
        return $this->respondNotFound('Fuel not deleted');
	}

}
