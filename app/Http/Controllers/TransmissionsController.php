<?php namespace App\Http\Controllers;

use App\Http\Requests\TransmissionRequest;
use App\Dealer\Repositories\Contracts\TransmissionInterface;
use App\Dealer\Transformers\TransmissionTransformer;
use Illuminate\Support\Facades\Request;

class TransmissionsController extends ApiController {

    /**
     * @var
     */
    protected $transmissionRepository;

    /**
     * @var
     */

    protected $transmissionTransformer;

    public function __construct(TransmissionInterface $transmissionRepository, TransmissionTransformer $transmissionTransformer){
        $this->transmissionRepository = $transmissionRepository;
        $this->transmissionTransformer = $transmissionTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->transmissionRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->transmissionRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->transmissionRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->transmissionTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No vehicle transmission found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->transmissionTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param TransmissionRequest $request
     * @return mixed
     */
    public function store(TransmissionRequest $request)
    {
        if($this->transmissionRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Vehicle transmission has been created.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $transmission = $this->transmissionRepository->getById($uuid);

        if(!$transmission)
        {
            return $this->respondNotFound('Transmission not found.');
        }

        return $this->respond([
            'data' => $this->transmissionTransformer->transform($transmission)
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param TransmissionRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(TransmissionRequest $request, $uuid)
    {
        if($this->transmissionRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Vehicle transmission has been updated.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
    }

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->transmissionRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Vehicle transmission has been deleted');
        }
        return $this->respondNotFound('Vehicle transmission not deleted');
    }

}
