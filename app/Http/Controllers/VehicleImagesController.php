<?php  namespace App\Http\Controllers;

use App\Dealer\Repositories\Contracts\VehicleImageInterface;
use App\Dealer\Transformers\VehicleImageTransformer;
use Illuminate\Support\Facades\Request;

class VehicleImagesController extends ApiController {

    /**
     * @var
     */
    protected $vehicleImageRepository;

    /**
     * @var
     */

    protected $vehicleImageTransformer;

    public function __construct(VehicleImageInterface $vehicleImageRepository, VehicleImageTransformer $vehicleImageTransformer){
        $this->vehicleImageRepository = $vehicleImageRepository;
        $this->vehicleImageTransformer = $vehicleImageTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->vehicleImageRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->vehicleImageRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->vehicleImageRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->vehicleImageTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No vehicleImages found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->vehicleImageTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
    }

}
