<?php namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;

class ApiController extends Controller{

    protected $statusCode = 200;

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * The response status code
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Gives the resource collection with pagination
     * @param LengthAwarePaginator $items
     * @param $data
     * @return mixed
     */

    protected function respondWithPagination(LengthAwarePaginator $items, $data)
    {
        $data = array_merge($data,[
            'paginator' => [
                'total_count' 	=> $items->total(),
                'total_pages' 	=> ceil($items->total() / $items->perPage()),
                'current_page'	=>	$items->currentPage(),
                'limit'			=>	$items->perPage()
            ]
        ]);

        return $this->respond($data);

    }

    /**
     * When a missing resource is requested
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = "Not Found !")
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * When a non supported search parameter is requested
     * @param string $message
     * @return mixed
     */
    public function respondWrongParameter ($message = "You requested a non supported search parameter!")
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * There was an internal error
     * @param string $message
     * @return mixed
     */
    public function respondInternalError($message = "Internal Server Error !!")
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }


    /**
     * Give json feedback with status code
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return \Response::json($data, $this->getStatusCode(), $headers);

    }

    /**
     * respond with a generic error
     * @param string $message
     * @return mixed
     */
    public function respondWithError($message  = 'There was an error')
    {
        return $this->respond([
            'error' => [
                'error'         => true,
                'message'       => $message,
                'status_code'   => $this->getStatusCode()
            ]
        ]);

    }

    /**
     * Some operation (save only?) has completed successfully
     * @param string $message
     * @return mixed
     */
    public function respondWithSuccess($message = 'Success !!')
    {
        return $this->respond( $message );
    }

    /**
     * Some operation (save) failed.
     * @param string $message
     * @return mixed
     */
    public function respondNotSaved($message = "Not Saved !")
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

}