<?php namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Dealer\Repositories\Contracts\CategoryInterface;
use App\Dealer\Transformers\CategoryTransformer;
use Illuminate\Support\Facades\Request;

class CategoriesController extends ApiController {

    /*
     * Repository variable
     */

    protected $categoryRepository;

    /*
     * Transformer variable
     */

    protected $categoryTransformer;

    public function __construct(CategoryInterface $categoryRepository, CategoryTransformer $categoryTransformer){
        $this->categoryRepository = $categoryRepository;
        $this->categoryTransformer = $categoryTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->categoryRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->categoryRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->categoryRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->categoryTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No categories found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->categoryTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);

}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param CategoryRequest $request
     * @return mixed
     */
    public function store(CategoryRequest $request)
	{
        if($this->categoryRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! category has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}


    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $category = $this->categoryRepository->getById($uuid);

        if(!$category)
        {
            return $this->respondNotFound('Category not found.');
        }

        return $this->respond([
            'data' => $this->categoryTransformer->transform($category)
        ]);
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $uuid
     */
    public function edit($uuid)
	{

	}

    /**
     * Update the specified resource in storage.
     * @param CategoryRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(CategoryRequest $request, $uuid)
	{
        if($this->categoryRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Category has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->categoryRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Category has been deleted');
        }
        return $this->respondNotFound('Category not deleted');
	}

}
