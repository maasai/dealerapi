<?php namespace App\Http\Controllers;

use App\Http\Requests\FeatureListingRequest;
use App\Dealer\Repositories\Contracts\FeatureListingInterface;
use App\Dealer\Transformers\FeatureListingTransformer;
use Illuminate\Support\Facades\Request;

class FeatureListingController extends ApiController {

    /**
     * @var
     */
    protected $featureListingRepository;

    /**
     * @var
     */

    protected $featureListingTransformer;


    public function __construct(FeatureListingInterface $featureListingRepository, FeatureListingTransformer $featureListingTransformer){
        $this->featureListingRepository = $featureListingRepository;
        $this->featureListingTransformer = $featureListingTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->featureListingRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->featureListingRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->featureListingRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->featureListingTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No feature - listings found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->featureListingTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param FeatureListingRequest $request
     * @return mixed
     */
    public function store(FeatureListingRequest $request)
	{
        if($this->featureListingRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Feature listing has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $featureListing = $this->featureListingRepository->getById($uuid);

        if(!$featureListing)
        {
            return $this->respondNotFound('Feature - Listing not found.');
        }

        return $this->respond([
            'data' => $this->featureListingTransformer->transform($featureListing)
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param FeatureListingRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(FeatureListingRequest $request, $uuid)
	{
        if($this->featureListingRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Feature listing has been updated.');
        }
        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->featureListingRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Feature listing has been deleted');
        }
        return $this->respondNotFound('Feature listing not deleted');
	}

}
