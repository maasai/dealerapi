<?php namespace App\Http\Controllers;

use App\Http\Requests\VehiclemodelRequest;
use App\Dealer\Repositories\Contracts\ModelInterface;
use App\Dealer\Transformers\ModelTransformer;
use Illuminate\Support\Facades\Request;

class VehicleModelsController extends ApiController {

    /**
     * @var
     */
    protected $modelRepository;

    /**
     * @var
     */

    protected $modelTransformer;

    public function __construct(ModelInterface $modelRepository, ModelTransformer $modelTransformer){
        $this->modelRepository = $modelRepository;
        $this->modelTransformer = $modelTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //retrieve current time of server before touching the database.
        $now = time();
        //The timestamp of the last sync call made by client
        // - if not available we assume its the first time the call is made
        //  and therefore provide no data to delete as the client has no data yet

        $modifiedSince = Request::input('modifiedSince', null);
        $syncData = $this->modelRepository->getAll();
        //Ids to desync
        $desync = null;

        if(null != $modifiedSince && is_numeric($modifiedSince)){
            //the client has timestamp
            // so return only records that have been touched since then
            $syncData = $this->modelRepository->getUpdated($modifiedSince);

            //send a list of ids to desync
            $desync = $this->modelRepository->getDeleted($modifiedSince);

        }else{
            return $this->respondWithPagination($syncData, [
                'data' => $this->modelTransformer->transformCollection($syncData->all()),
                'timestamp' => $now
            ]);
        }

        //send only the ids for the records to be deleted
        if(isset($desync)){
            $desync = $desync->fetch('uuid')->toArray();
        }

        if(null == $syncData){
            return $this->respondNotFound('No vehicle model found');
        }

        return $this->respondWithPagination($syncData, [
            'data' => $this->modelTransformer->transformCollection($syncData->all()),
            'timestamp' => $now,
            'desync'    => $desync
        ]);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created resource in storage.
     * @param VehiclemodelRequest $request
     * @return mixed
     */
    public function store(vehiclemodelRequest $request)
	{
        if($this->modelRepository->create($request->all())){
            return $this->respondWithSuccess('Success !! Vehicle model has been created.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Display the specified resource.
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
	{
        $model = $this->modelRepository->getById($uuid);

        if(!$model)
        {
            return $this->respondNotFound('Model not found.');
        }

        return $this->respond([
            'data' => $this->modelTransformer->transform($model)
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     * @param VehiclemodelRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(VehiclemodelRequest $request, $uuid)
	{
        if($this->modelRepository->update($request->all(), $uuid)){
            return $this->respondWithSuccess('Success !! Vehicle model has been updated.');
        }

        return $this->respondNotSaved("Unexpected error just happened");
	}

    /**
     * Remove the specified resource from storage.
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
	{
        if($this->modelRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Vehicle model has been deleted');
        }
        return $this->respondNotFound('Vehicle model not deleted');
	}

}
