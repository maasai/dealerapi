<?php  namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ColorRequest extends FormRequest {

    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }

    /**
     * Rules to validate our resource
     * @return array
     */

    public function rules()
    {
        $rules = [
            'color_name' => 'required|unique:colors,color_name'
        ];

        if($id = $this->colors){
            $rules['color_name'] .= ','.$id.',uuid';
        }
        return $rules;
    }

    /**
     * Authorized to perform this?
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
} 