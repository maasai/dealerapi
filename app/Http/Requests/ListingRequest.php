<?php namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;

class ListingRequest extends Request {
    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $rules = [
            'category_id'      => 'required',
            'condition_id'     => 'required',
            'fuel_id'          => 'required',
            'make_id'          => 'required',
            'mileage'          => 'required',
            'model_id'         => 'required',
            'price'            => 'required',
            'registration_year' => 'required',
            'transmission_id'  => 'required',
            'doors'             => 'required',
            'engine_size'       => 'required',
            'vin_number'        => 'required',
            'exterior_color_id' => 'required',
            'interior_color_id' => 'required',
			'features' => 'required'
        ];

        for($imageCount = 1; $imageCount <= 5; $imageCount++){
            $image_name = 'image_'.$imageCount;
            if(isset($_FILES[$image_name])){
                $rules[$image_name] = 'image|mimes:jpg,jpeg,png';
                if(!$this->listings){
                  $rules[$image_name] = 'required|image|mimes:jpg,jpeg,png';
                }
            }
        }
        return $rules;
	}

}
