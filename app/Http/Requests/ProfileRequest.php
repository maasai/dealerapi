<?php namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;

class ProfileRequest extends Request {
    /**
     * Overrides response from the FormRequest
     * to not redirect for our API development
     * @param array $errors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $message = array(
            'message' => "There were validation errors",
            'errors' => $errors
        );

        return new JsonResponse($message, 400);
    }

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'username' => 'required|unique:users,username,'.\Auth::user()->uuid.',uuid',
            'email'    => 'required|email|unique:users,email,'.\Auth::user()->uuid.',uuid',
            'name'     => 'required',
            'password' => 'confirmed|min:6',
            'photo'    => 'image|mimes:jpg,jpeg,png',
        ];
	}

}
