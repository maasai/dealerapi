<?php

function getCounter($paginator){
    $counter =  ($paginator['current_page'] - 1) * $paginator['limit'] + 1;
    return $counter;
}


function formatMoney($amount){
    $settings = App\Setting::first();
    $symbol = $settings ? $settings->currency : '';
    $formatted_amount = $symbol.number_format($amount, 2);
    return $formatted_amount;
}

function formatDate($date){
    $settings = App\Setting::first();
    $formatted_date = $settings ? date($settings->date_format, strtotime($date))  : $date;
    return $formatted_date;
}