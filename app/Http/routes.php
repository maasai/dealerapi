<?php

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

#Installation script Routes
Route::group(array('prefix'=>'install','before'=>'install'),function()
{
    Route::get('/','Backend\InstallController@index');
    Route::get('/database','Backend\InstallController@getDatabase');
    Route::post('/database','Backend\InstallController@postDatabase');
    Route::get('/user','Backend\InstallController@getUser');
    Route::post('/user','Backend\InstallController@postUser');
});

//Route::group(['middleware' => 'install'], function(){
Route::group(['middleware' => 'auth'], function(){
    Route::get('/', 'Backend\DashboardController@index');
    Route::get('home', 'Backend\DashboardController@index');
    Route::group(array('prefix'=>'backend', 'middleware' => 'auth' ),function()
    {
        Route::resource('colors', 'Backend\ColorsController');
        Route::resource('categories', 'Backend\CategoriesController');
        Route::resource('conditions', 'Backend\ConditionsController');
        Route::resource('features', 'Backend\FeaturesController');
        Route::resource('fuels', 'Backend\FuelsController');
        Route::resource('makes', 'Backend\MakesController');
        Route::resource('transmissions', 'Backend\TransmissionsController');
        Route::resource('models', 'Backend\VehicleModelsController');
        Route::resource('listings', 'Backend\ListingsController');
        Route::post('listings/getModels', 'Backend\ListingsController@getModels');
        Route::resource('images', 'Backend\ImagesController');
        Route::resource('users', 'Backend\UsersController');
        Route::resource('settings', 'Backend\SettingsController');
        # Profile
        Route::get('profile', 'Backend\UsersController@editProfile');
        Route::post('profile', 'Backend\UsersController@updateProfile');
        #listings
        Route::get('listingsFilter', 'Backend\ListingsController@filter');
        #models
        Route::get('modelsFilter', 'Backend\VehicleModelsController@filter');
    });
});
//});
Route::resource('colors', 'ColorsController');
Route::resource('categories', 'CategoriesController');
Route::resource('conditions', 'ConditionsController');
Route::resource('features', 'FeaturesController');
Route::resource('fuels', 'FuelsController');
Route::resource('makes', 'MakesController');
Route::resource('transmissions', 'TransmissionsController');
Route::resource('models', 'VehicleModelsController');
Route::resource('listings', 'ListingsController');
Route::resource('feature_listings', 'FeatureListingController');
Route::resource('vehicle_images', 'VehicleImagesController');
Route::get('settings', 'SettingsController@index');


//protected routes
Route::group(array('prefix' => 'api/v1', 'before' => 'oauth'), function () {
    Route::resource('colors', 'ColorsController');
});
//Route::resource('colors', 'ColorsController', array('only' => array('index', 'show')));


//Respond to incoming access token requests
Route::post('api/v1/oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});
