<?php namespace App;


class VehicleImage extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vehicle_images';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array(
        'uuid','filename',  'listing_id'
    );

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

    public function listing(){
        return $this->belongsTo('App\Listing');
    }

}
