<?php namespace App;

class Condition extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conditions';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array('uuid','condition_name');

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

}
