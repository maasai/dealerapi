<?php namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;

class VehicleModel extends BaseModel {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vehicle_models';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Entities fields to be mass assigned
     * @var array
     */
    protected $fillable = array('uuid','model_name',  'make_id');

    /**
     * Date fields to be converted to carbon objects
     * @var bool
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /* Relationships */

    public function make(){
        return $this->belongsTo('App\Make','make_id','uuid');
    }


}
