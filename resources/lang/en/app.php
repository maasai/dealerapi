<?php

return [
    /*|--------------------------------------------------------------------------
    | DASHBOARD
    |--------------------------------------------------------------------------*/
    'dashboard_title'     => 'Dashboard',
    'vehicle_listings'    => 'Vehicle Listings',
    'vehicle_makes'       => 'Vehicle Makes',
    'vehicle_models'      => 'Vehicle Models',
    'vehicle_categories'  => 'Vehicle Categories',
    'latest_listings'     => 'Latest Listings',

    'login'     => 'Login',
    'logout'    => 'Logout',
    'online'    => 'Online',
    'profile'   => 'Profile',
    'edit_profile'   => 'Edit Profile',
    'profile_photo'   => 'Profile Photo',
    'save'          =>'Save',
    'close'     => 'Close',

    /*|--------------------------------------------------------------------------
	| Main Menu
	|--------------------------------------------------------------------------*/
    'main_navigation'   => 'MAIN NAVIGATION',
    'dashboard'         => 'Dashboard',
    'listings'          => 'Listings',
    'settings_menu'     => 'Settings',
    'colors_menu'       => 'Colors',
    'categories_menu'   => 'Categories',
    'conditions_menu'   => 'Conditions',
    'features_menu'     => 'Features',
    'fuels_menu'        => 'Fuel Types',
    'makes_menu'        => 'Makes',
    'transmissions_menu'=> 'Transmissions',
    'users_menu'        => 'Users',
    'models_menu'        => 'Models',
    'inventory_header'  => 'INVENTORY',
    'config_header'     => 'CONFIGURATION',

    /*|--------------------------------------------------------------------------
	| CRUD
	|--------------------------------------------------------------------------*/
    'view'      => 'View',
    'edit'      => 'Edit',
    'delete'    => 'Delete',
    'delete_multiple'    => 'Delete Multiple',
    'add_btn'   => 'Add',
    /*|--------------------------------------------------------------------------
	| LISTINGS
	|--------------------------------------------------------------------------*/
    'add_listing_btn'           => 'ADD LISTING',
    'edit_listing_title'        => 'EDIT LISTING',
    'back_listing_btn'          => 'BACK TO LISTINGs',
    'listings_view_title'       => 'VIEW LISTING',
    'photo'                     => 'Photo',
    'details'                   => 'Listing Details',
    'price'                     => 'Price',
    'created'                   => 'Created',
    'action'                    => 'Action',
    'listings_title'            => 'Listings',
    'make_label'                => 'Make',
    'model_label'               => 'Model',
    'mileage_label'             => 'Mileage',
    'price_label'               => 'Price',
    'color_label'               => 'Color',
    'details_label'             => 'Extra Details',
    'condition_label'           => 'Condition',
    'category_label'            => 'Category',
    'featured_listing_label'    => 'Featured Listing',
    'transmission_label'        => 'Transmission',
    'registration_label'        => 'Registration Year',
    'fuel_label'                => 'Fuel',
    'active_label'              => 'Active Listing',
    'images_label'              => 'Images',
    'save_listing'              => 'Save Listing',
    'add_listing_title'         => 'Add Listing',
    'back_to_listings_title'    => 'BACK TO LISTINGS',
    'featured_txt'              => 'Featured',
    'active_txt'                => 'Active',
    'browse_txt'                => 'Browse',
    'features_label'            => 'Features',
    'vehicle_details_label'     => 'Vehicle  Details',
    'vin_number_label'          => 'VIN Number',
    'engine_size_label'         => 'Engine Size',
    'doors_label'               => 'Number of Doors',
    'exterior_color_label'      => 'Exterior Color',
    'interior_color_label'      => 'Interior Color',
    'select_all_label'          => 'Select All',
    'filter_listings'           => 'Filter Listings',
    /*|--------------------------------------------------------------------------
	| COLORS
	|--------------------------------------------------------------------------*/
    'colors_title'                  => 'Colors',
    'color_name'                    => 'Color Name',
    'add_color_title'               => 'Add Color',
    /*|--------------------------------------------------------------------------
	| CATEGORIES
	|--------------------------------------------------------------------------*/
    'categories_title'              => 'Categories',
    'category_name'                 => 'Category Name',
    'add_category_title'            => 'Add Category',
    /*|--------------------------------------------------------------------------
	| CONDITIONS
	|--------------------------------------------------------------------------*/
    'conditions_title'              => 'Conditions',
    'condition_name'                => 'Condition Name',
    'add_condition_title'           => 'Add Condition',
    /*|--------------------------------------------------------------------------
   | FEATURES
   |--------------------------------------------------------------------------*/
    'features_title'              => 'Features',
    'feature_name'                => 'Feature Name',
    'add_feature_title'           => 'Add Feature',
  /*|--------------------------------------------------------------------------
  | FUELS TYPES
  |--------------------------------------------------------------------------*/
    'fuels_title'              => 'Fuel Types',
    'fuel_name'                => 'Fuel Name',
    'add_fuel_title'           => 'Add Fuel Type',
    /*---------------------------------------------------------------------------
     | MAKES
     |--------------------------------------------------------------------------*/
    'makes_title'              => 'Vehicle Makes',
    'make_name'                => 'Make Name',
    'add_make_title'           => 'Add Make',
    /*---------------------------------------------------------------------------
     | MODELS
     |--------------------------------------------------------------------------*/
    'models_title'              => 'Vehicle Models',
    'model_name'                => 'Model Name',
    'add_model_title'           => 'Add Model',
    'edit_model_title'          => 'Edit Model',
    'filter_models'             => 'Filter Models',
    /*---------------------------------------------------------------------------
     | Transmissions
     |--------------------------------------------------------------------------*/
    'transmissions_title'        => 'Transmission',
    'transmission_name'          => 'Transmission Name',
    'add_transmission_title'     => 'Add Transmission',
    /*---------------------------------------------------------------------------
     | USERS
     |--------------------------------------------------------------------------*/
    'user_name_label'            => 'Name',
    'email_label'                => 'Email',
    'phone_label'                => 'Phone',
    'username_label'             => 'Username',
    'password_label'             => 'Password',
    'confirm_password_label'     => 'Confirm Password',
    'add_user_title'             => 'Add User',
    'edit_user_title'            => 'Edit User',
    'users_title'                => 'Users',
    /*|--------------------------------------------------------------------------
   | SETTINGS
   |--------------------------------------------------------------------------*/
    'settings_title'                => 'Settings',
    'company_name_label'            => 'Company Name',
    'company_email_label'           => ' Email',
    'company_address_label'         => ' Address',
    'company_city_label'            => ' City',
    'company_state_label'           => ' State',
    'company_postal_code_label'     => ' Postal Code',
    'company_telephone_label'       => ' Telephone',
    'company_mileage_label'         => 'Mileage Units',
    'company_currency_label'        => 'Currency Symbol',
    'company_longitude_label'       => 'Longitude',
    'company_latitude_label'        => 'Latitude',
    'company_facebook_label'        => 'Facebook',
    'company_twitter_label'         => 'Twitter',
    'company_website_label'         => 'Website',
    'company_logo_label'            => 'Company Logo',
    'date_format_label'             => 'Date Format',
    'save_settings'                 => 'Save Settings',
    'company_description_label'     => 'Description',
];