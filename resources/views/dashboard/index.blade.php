@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <h3><i class="fa fa-dashboard"></i> @Lang('app.dashboard_title') </h3>
</div>
<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-tasks"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">@Lang('app.vehicle_listings')</span>
                    <span class="info-box-number">{{ $stats['listings'] }}</span>

                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-car"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">@Lang('app.vehicle_makes')</span>
                    <span class="info-box-number">{{ $stats['makes'] }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-bus"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">@Lang('app.vehicle_models')</span>
                    <span class="info-box-number">{{ $stats['models'] }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-ellipsis-h"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">@Lang('app.vehicle_categories')</span>
                    <span class="info-box-number">{{ $stats['categories'] }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col -->
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3>@Lang('app.latest_listings')</h3>
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th></th>
                            <th width="15%">@Lang('app.photo')</th>
                            <th width="20%">@Lang('app.details')</th>
                            <th width="10%">@Lang('app.price')</th>
                            <th width="10%">@Lang('app.created')</th>
                            <th>@Lang('app.action')</th>
                        </tr>
                        <tbody id="listing_body">
                        <?php $i = ($data['paginator']['current_page'] - 1) * $data['paginator']['limit'] + 1; ?>
                        @foreach($data['listings'] as $listing)
                        <tr>
                            <td>{{ $i }}</td>
                            <td><img src="{{ isset($listing['images'][0]) && $listing['images'][0]['filename'] != '' ? asset('assets/img/vehicle/thumb/'.$listing['images'][0]['filename']) : asset('assets/img/vehicle/no-image.jpg') }}" width="50%"/></td>
                            <td>
                                {!! '<strong>'.$listing['make']['make_name'].' '.$listing['model']['model_name'].'</strong> ('.$listing['registration_year'].')<br/>'  !!}
                                {!! 'Condition: '.$listing['condition']['condition_name'].'<br/>Transmission: '.$listing['transmission']['transmission_name'].'<br/>'  !!}
                                {!! 'Fuel: '.$listing['fuel']['fuel_name'] !!}
                            </td>
                            <td>{!! '<strong>'.formatMoney($listing['price']).'</strong>' !!}</td>
                            <td>{!! formatDate($listing['created_at']).'</strong>' !!}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ route('backend.listings.show', $listing['uuid']) }}"><i class="fa fa-eye"></i> @Lang('app.view')</a>
                                <a class="btn btn-success btn-sm" href="{{ route('backend.listings.edit', $listing['uuid']) }}"><i class="fa fa-pencil"></i> @Lang('app.edit')</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody></table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@endsection
