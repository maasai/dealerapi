@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-car"></i> @Lang('app.makes_title') </h3>
    </div>
    <div class="col-md-6">
        <h3><a class="btn btn-primary pull-right" data-toggle="ajax-modal" href="{{ route('backend.makes.create') }}"><i class="fa fa-plus"></i> @Lang('app.add_btn')</a></h3>
    </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">

                        <tr>
                            <th width="5%">Count</th>
                            <th width="10%">
                                <div class="radio">
                                    <label for="select_all"> {!! Form::checkbox('select_all','1',null,['class'=>'minimal', 'id'=>'select_all']) !!} </label> &nbsp;
                                </div>
                            </th>
                            <th width="15%">@Lang('app.make_name')</th>
                            <th>@Lang('app.action')</th>
                        </tr>
                        <tbody id="listing_body">
                        <?php $counter = getCounter($data['paginator']); ?>
                        @foreach($data['makes'] as $count => $make)
                        <tr>
                            <td>{{ $counter }}</td>
                            <td>
                                <div class="radio">
                                    <label for="{{$make['uuid']}}"> {!! Form::checkbox('selected',$make['uuid'],null,['class'=>'minimal select_record']) !!} </label> &nbsp;
                                </div>
                            </td>
                            <td><a class="xedit" data-field-name="make_name" data-url="{{ route('backend.makes.update', $make->uuid) }}" data-title="Edit make Name"> {{ $make->make_name }} </a></td>
                            <td>
                                {!! Form::open(array("method"=>"DELETE", "route" => ['backend.makes.destroy', $make['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                    <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i>  @Lang('app.delete')</a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        <?php $counter++ ?>
                        @endforeach
                        </tbody></table>
                    <div  id="paginationWrapper" style="text-align: center">
                        {!! str_replace('/?', '?', $data['paginator']['links']) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
                    {!! Form::open(array("method"=>"DELETE", "route" => ['backend.makes.destroy', ''], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                    <a class="btn btn-danger btn-sm btn-delete-multiple"><i class="fa fa-trash"></i> @Lang('app.delete_multiple') </a>
                    {!! Form::close() !!}

        </div>
    </div>
</section>
@endsection
@section ('js_scripts')
        @include('makes.partials._makes_js')
@endsection