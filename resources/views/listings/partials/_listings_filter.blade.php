<?php foreach($data['listings'] as $count => $listing) { ?>
    <tr>
        <td><?php echo $count+1; ?></td>
        <td>
            <div class="radio">
                <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
            </div>
        </td>
        <td><img src="<?php echo isset($listing['images'][0]) && $listing['images'][0]['filename'] != '' ? asset('assets/img/vehicle/thumb/'.$listing['images'][0]['filename']) : asset('assets/img/vehicle/no-image.jpg'); ?>" width="50%"/></td>
        <td>
            <?php echo '<strong>'.$listing['make']['make_name'].' '.$listing['model']['model_name'].'</strong> ('.$listing['registration_year'].')<br/>'  ?>
            <?php echo 'Condition: '.$listing['condition']['condition_name'].'<br/>Transmission: '.$listing['transmission']['transmission_name'].'<br/>'  ?>
            <?php echo 'Fuel: '.$listing['fuel']['fuel_name'] ?>
        </td>
        <td><?php echo '<strong>'.number_format($listing['price'],2).'</strong>';?></td>
        <td><?php echo date('d M Y', strtotime($listing['created_at'])).'</strong>'; ?></td>
        <td>
            <a class="btn btn-primary btn-sm" href="<?php echo route('backend.listings.show', $listing['uuid']) ?>"><i class="fa fa-eye"></i> <?php echo Lang::get('app.view'); ?></a>
            <a class="btn btn-success btn-sm" href="<?php echo route('backend.listings.edit', $listing['uuid']) ?>"><i class="fa fa-pencil"></i> <?php echo Lang::get('app.edit'); ?></a>
            <?php echo Form::open(array("method"=>"DELETE", "route" => ['backend.listings.destroy', $listing['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i>'.Lang::get("app.delete").'</a>'.Form::close(); ?>
        </td>
    </tr>
<?php } ?>