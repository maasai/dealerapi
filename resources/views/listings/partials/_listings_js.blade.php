<script type="text/javascript">
$(function(){
    /*======================================
     CUSTOM SCRIPT TO GET VEHICLE MODELS
     ========================================*/
    $(document).on('change', '#make_id', function(){
        var element = $('#model_id');
        if(element.hasClass('chzn-done')){
            //remove chosen
            element.next('[id=model_id_chosen]').remove(); //Make sure its id contain _chzn
            //remove chosen class in original combobox and make it visible
            element.removeClass('chzn-done').css('display','block');
        }
        $('#model_id_chosen').addClass('spinner');
        var make = $('#make_id').val();
        $.post('{{ url("backend/listings/getModels") }}', {make : make}, 'json').done(function(data){
            var options = '';
            $.each( data.models, function( key, value ) {
                options += '<option value="'+ key +'">' + value + '</option>';
            });
            $('#model_id').html(options).trigger("chosen:updated");

        }).always(function(){
            $('#model_id_chosen').removeClass('spinner');
        });

    });

    /*======================================
     CUSTOM SCRIPT TO filter listings
     ========================================*/
    $(document).on('click', '#filter_listings', function(e){
        e.preventDefault();
        var make = $('#make_id').val();
        var model = $('#model_id').val();
        if(make != ''){
                $.ajax({
                    url:'{{ url('backend/listingsFilter') }}',
                    data: {make:make, model: model},
                    beforeSend: function(){
                        $('#filter_listings').parents('.content').addClass('spinner');
                    },
                    success: function(data){
                        $('#listings_tbl').dataTable().fnDestroy();
                        $('#listing_body').html(data.html);
                        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                            checkboxClass: 'icheckbox_minimal-blue',
                            radioClass: 'iradio_minimal-blue'
                        });
                        $('#paginationWrapper').addClass('hide');
                        $('#listings_tbl').dataTable();
                    },
                    complete:function(){
                        $('#filter_listings').parents('.content').removeClass('spinner');
                    }
                });
        }
    });
});

function deleteImg(imageId){
    url = '{{ url('backend/images') }}/'+imageId;
    BootstrapDialog.show({
        title: 'Deleting a Record',
        message: 'You are about to delete a record. This action cannot be undone. Do you want to proceed?',
        buttons: [ {
            icon: 'fa fa-check',
            label: ' Yes',
            cssClass: 'btn-success btn-sm',
            action: function(){
                $.post(url, { _method: "delete" }, 'json').done(function(data){
                    //remove the image from frontend
                    $('div#'+imageId).remove();
                }).always(function(){
                    $('.bootstrap-dialog-close-button').click();
                });
            }
        }, {
            icon: 'fa fa-remove',
            label: ' No',
            cssClass: 'btn-danger btn-sm',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
    return false;
}
</script>
