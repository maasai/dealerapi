@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-tasks"></i> @Lang('app.listings_view_title') </h3>
    </div>
    <div class="col-md-6">
        <h3><a class="btn btn-primary pull-right" href="{{ route('backend.listings.index') }}">@Lang('app.back_listing_btn')</a></h3>
    </div>
</div>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <section class="content-header">
                <h1>{{ $listing->registration_year.' '.$listing->make->make_name.' '.$listing->model->model_name }}</h1>
            </section>
            <div class="box box-primary">
                <div class="box-header"></div>
                <div class="box-body table-responsive no-padding">
                    <div class="col-xs-12">
                        <strong><p>{{ Lang::get('app.images_label') }}</p></strong>
                        @foreach ($listing['images'] as $image)
                        <div class="col-lg-2 thumb">
                            <a class="thumbnail" href="#">
                                <img src="{{ asset('assets/img/vehicle/thumb/'.$image->filename) }}" width="100%"/>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-xs-12">
                        <p><strong>{{ Lang::get('app.vehicle_details_label') }}</strong></p>
                    </div>
                    <div class="col-xs-4">
                        <table class="table table-striped">

                            <tbody><tr>
                                <td>{{ Lang::get('app.make_label') }}</td>
                                <td>{{ $listing->make->make_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.model_label') }}</td>
                                <td>{{ $listing->model->model_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.condition_label') }}</td>
                                <td>{{ $listing->condition->condition_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.mileage_label') }}</td>
                                <td>{{ $listing->mileage }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.vin_number_label') }}</td>
                                <td>{{ $listing->vin_number}}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.registration_label') }}</td>
                                <td>{{ $listing->registration_year}}</td>
                            </tr>
                            </tbody></table>
                    </div>
                    <div class="col-xs-4">
                        <table class="table table-striped">

                            <tbody><tr>
                                <td>{{ Lang::get('app.price_label') }}</td>
                                <td>{{ formatMoney($listing->price) }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.exterior_color_label') }}</td>
                                <td>{{ $listing->exterior_color->color_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.interior_color_label') }}</td>
                                <td>{{ $listing->interior_color->color_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.engine_size_label') }}</td>
                                <td>{{ $listing->engine_size }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.category_label') }}</td>
                                <td>{{ $listing->category->category_name}}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.details_label') }}</td>
                                <td>{{ $listing->extra_details}}</td>
                            </tr>
                            </tbody></table>
                    </div>
                    <div class="col-xs-4">
                        <table class="table table-striped">

                            <tbody><tr>
                                <td>{{ Lang::get('app.transmission_label') }}</td>
                                <td>{{ $listing->transmission->transmission_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.doors_label') }}</td>
                                <td>{{ $listing->doors }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.fuel_label') }}</td>
                                <td>{{ $listing->fuel->fuel_name }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.active_label') }}</td>
                                <td>{{ $listing->active ? 'Yes' : 'No' }}</td>
                            </tr>
                            <tr>
                                <td>{{ Lang::get('app.featured_listing_label') }}</td>
                                <td>{{ $listing->featured ? 'Yes' : 'No' }}</td>
                            </tr>
                            </tbody></table>
                    </div>
                    <div class="col-xs-12">
                        <p><strong>{{ Lang::get('app.features_label') }}</strong></p>
                        @foreach($listing['featureListings'] as $feature)
                        <div class="form-group col-md-2" style="margin-bottom: 0;">
                            <div class="checkbox">
                                <div class="" style="margin-top:5px">
                                    <i class="fa fa-check"></i>
                                    {{ $feature->feature->feature_name }}

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>




    </section>
@endsection