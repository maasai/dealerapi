@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-tasks pull-left"></i> @Lang('app.listings_title') </h3>
    </div>
    <div class="col-md-6">
        <h3><a class="btn btn-primary pull-right" href="{{ route('backend.listings.create') }}">@Lang('app.add_listing_btn')</a></h3>
    </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12" style="padding-bottom: 10px">
            <form class="form-inline">
                <div class="form-group col-xs-3">
                    {!! Form::label('make', 'Make') !!}
                    {!! Form::select('make_id',$makes, null, ['class' => 'form-control input-sm chosen','required','id'=>'make_id']) !!}
                </div>
                <div class="form-group  col-xs-3">
                    {!! Form::label('make', ' Model') !!}
                    {!! Form::select('model_id',array(), null, ['class' => 'form-control chosen','id'=>'model_id']) !!}
                </div>
                <div class="form-group  col-xs-3" style="padding-top: 25px">
                    <button type="submit" class="btn btn-success" id="filter_listings"> <i class="fa fa-search"></i> {{ Lang::get('app.filter_listings') }}</button>
                </div>
            </form>
        </div>
   </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header"></div>
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                <div class="box-body no-padding">
                    <table class="table table-hover table-striped" id="listings_tbl">
                        <thead>
                            <tr>
                                <th></th>
                                <th width="10%">
                                    <div class="radio">
                                        <label for="select_all"> {!! Form::checkbox('select_all','1',null,['class'=>'minimal', 'id'=>'select_all']) !!} </label> &nbsp;
                                    </div>
                                </th>
                                <th width="15%">@Lang('app.photo')</th>
                                <th width="20%">@Lang('app.details')</th>
                                <th width="10%">@Lang('app.price')</th>
                                <th width="10%">@Lang('app.created')</th>
                                <th>@Lang('app.action')</th>
                            </tr>
                        </thead>
                        <tbody id="listing_body">
                        <?php $counter = ($data['paginator']['current_page'] - 1) * $data['paginator']['limit'] + 1; ?>
                        @foreach($data['listings'] as $listing)
                        <tr>
                            <td>{{ $counter }}</td>
                            <td>
                                <div class="radio">
                                    <label for="{{$listing['uuid']}}"> {!! Form::checkbox('selected',$listing['uuid'],null,['class'=>'minimal select_record']) !!} </label> &nbsp;
                                </div>
                            </td>
                            <td><img src="{{ isset($listing['images'][0]) && $listing['images'][0]['filename'] != '' ? asset('assets/img/vehicle/thumb/'.$listing['images'][0]['filename']) : asset('assets/img/vehicle/no-image.jpg') }}" width="50%"/></td>
                            <td>
                                {!! '<strong>'.$listing['make']['make_name'].' '.$listing['model']['model_name'].'</strong> ('.$listing['registration_year'].')<br/>'  !!}
                                {!! 'Condition: '.$listing['condition']['condition_name'].'<br/>Transmission: '.$listing['transmission']['transmission_name'].'<br/>'  !!}
                                {!! 'Fuel: '.$listing['fuel']['fuel_name'] !!}
                            </td>
                            <td>{!! '<strong>'.formatMoney($listing['price']).'</strong>' !!}</td>
                            <td>{!! formatDate($listing['created_at']).'</strong>' !!}</td>
                            <td>
                                <a class="btn btn-primary btn-sm" href="{{ route('backend.listings.show', $listing['uuid']) }}"><i class="fa fa-eye"></i> @Lang('app.view')</a>
                                <a class="btn btn-success btn-sm" href="{{ route('backend.listings.edit', $listing['uuid']) }}"><i class="fa fa-pencil"></i> @Lang('app.edit')</a>
                                {!! Form::open(array("method"=>"DELETE", "route" => ['backend.listings.destroy', $listing['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                    <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i>  @Lang('app.delete')</a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        <?php $counter++; ?>
                        @endforeach
                        </tbody></table>
                        <div  id="paginationWrapper" style="text-align: center">
                            {!! str_replace('/?', '?', $data['paginator']['links']) !!}
                        </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
                    {!! Form::open(array("method"=>"DELETE", "route" => ['backend.listings.destroy', ''], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                    <a class="btn btn-danger btn-sm btn-delete-multiple"><i class="fa fa-trash"></i> @Lang('app.delete_multiple') </a>
                    {!! Form::close() !!}


        </div>
    </div>
</section>
@endsection
@section ('js_scripts')
@include('listings.partials._listings_js')
@endsection