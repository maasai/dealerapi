@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-tasks"></i> {{ Lang::get('app.edit_listing_title') }}</h3>
    </div>
    <div class="col-md-6">
        <h3><a class="btn btn-primary pull-right" href="{{ route('backend.listings.index') }}">{{ Lang::get('app.back_to_listings_title') }}</a></h3>
    </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header"></div>
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                {!! Form::model($listing, ['route'=> ['backend.listings.update', $listing->uuid], 'method' => 'PATCH', 'class'=> 'ajax-submit', 'files'=>'true']) !!}
                <div class="box-body table-responsive no-padding">
                    <div class="feedback col-md-12"></div>
                    <div class="col-md-4">
                        <!-- text input -->
                        <div class="form-group">
                            {!! Form::label('make_id', Lang::get('app.make_label')) !!}
                            {!! Form::select('make_id',$makes, null, ['class' => 'form-control input-sm chosen','required','id'=>'make_id']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('mileage', Lang::get('app.mileage_label')) !!}
                            <div class="input-group">
                                {!! Form::input('number','mileage',null, ['class' => 'form-control input-sm','required','min'=>'0', 'placeholder'=>'Enter Mileage']) !!}
                                <span class="input-group-addon">KM</span>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('vin_number', Lang::get('app.vin_number_label')) !!}
                            {!! Form::text('vin_number',null, ['class' => 'form-control input-sm','required', 'placeholder'=>'Enter VIN Number']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('price', Lang::get('app.price_label')) !!}
                            {!! Form::input('number','price',null, ['class' => 'form-control input-sm','required','min'=>'0', 'placeholder'=>'Enter Price']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('exterior_color_id', Lang::get('app.exterior_color_label')) !!}
                            {!! Form::select('exterior_color_id',$colors, null, ['class' => 'form-control input-sm chosen','required']) !!}
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('model_id', Lang::get('app.model_label')) !!}
                            {!! Form::select('model_id',array($listing->model_id => $listing->model->model_name), $listing->model_id, ['class' => 'form-control input-sm chosen','required','id'=>'model_id']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('engine_size', Lang::get('app.engine_size_label')) !!}
                            {!! Form::text('engine_size',null, ['class' => 'form-control input-sm','required', 'placeholder'=>'Enter Engine Size']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('condition_id', Lang::get('app.condition_label')) !!}
                            {!! Form::select('condition_id',$conditions, null, ['class' => 'form-control input-sm  chosen','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('category_id', Lang::get('app.category_label')) !!}
                            {!! Form::select('category_id',$categories, null, ['class' => 'form-control input-sm chosen','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('interior_color_id', Lang::get('app.interior_color_label')) !!}
                            {!! Form::select('interior_color_id',$colors, null, ['class' => 'form-control input-sm chosen','required']) !!}
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('transmission_id', Lang::get('app.transmission_label')) !!}
                            {!! Form::select('transmission_id',$transmissions, null, ['class' => 'form-control input-sm chosen','required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('doors', Lang::get('app.doors_label')) !!}
                            {!! Form::input('number','doors',null, ['class' => 'form-control input-sm','required','min'=>'0', 'placeholder'=>'Enter number of doors']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('registration_year', Lang::get('app.registration_label')) !!}
                            <?php
                            $years_array  = array('' => 'Enter Registration Year');
                            for($year = date('Y'); $year  >=1900 ; $year--){
                                $years_array[$year] = $year;
                            }
                            ?>
                            {!! Form::select('registration_year',$years_array,null, ['class' => 'form-control input-sm chosen','required', 'placeholder'=>'Enter Registration Year']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('fuel_id', Lang::get('app.fuel_label')) !!}
                            {!! Form::select('fuel_id',$fuels, null, ['class' => 'form-control input-sm  chosen','required']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('active', Lang::get('app.active_label')) !!}
                            <div class="checkbox">
                                <?php $checked = $listing->active ? true : false; ?>
                                <label for="active"> {!! Form::checkbox('active','1',$checked,['class'=>'minimal']) !!} {{ Lang::get('app.active_txt') }}</label> &nbsp;
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('featured', Lang::get('app.featured_listing_label')) !!}
                            <div class="checkbox">
                                <?php $checked = $listing->featured ? true : false; ?>
                                <label for="featured"> {!! Form::checkbox('featured','1',$checked,['class'=>'minimal']) !!} {{ Lang::get('app.featured_txt') }}</label> &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('extra_details', Lang::get('app.details_label')) !!}
                            {!! Form::textarea('extra_details',null, ['class' => 'form-control editor']) !!}
                        </div>
                    </div>
                    <div class="form-group col-md-12" style="margin-bottom: 0">
                        <div class="alert alert-danger hide" id="features_error"></div>
                        {!! Form::label('feature', Lang::get('app.features_label')) !!}
                        <div class="checkbox">
                            <div style="margin-top:5px">
                                <label for="select_all"> {!! Form::checkbox('select_all','1',null,['class'=>'minimal', 'id'=>'select_all']) !!} <span id="label_select_all"> {{ Lang::get('app.select_all_label') }}</span></label>   &nbsp;
                            </div>
                        </div>
                    </div>


                    <?php
                    $selected = array();
                    foreach($listing['featureListings'] as $feat){
                        if(!in_array($feat->feature_id, $selected)){
                            array_push($selected,$feat->feature_id );
                        }
                    }
                    ?>
                    <div class="col-md-12">
                            @foreach($features_select as $feature)
                            <div class="form-group col-md-2" style="margin-bottom: 0;padding-left: 0">
                                <div class="checkbox">
                                    <div class="" style="margin-top:5px">
                                         <?php $checked = in_array($feature->uuid, $selected) ? true : false; ?>
                                        <label for="{{ $feature->uuid }}"> {!! Form::checkbox('features['.$feature->uuid.']',$feature->uuid,$checked,['class'=>'minimal select_record', 'id'=>$feature->uuid]) !!} {{ $feature->feature_name }}</label>
                                    </div>
                                </div>
                             </div>
                            @endforeach
                        </div>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        {!! Form::label('image', 'Images - ') !!}<i>max 5 images</i>
                        <div class="clearfix"></div>
                        <?php $count_img  = 0; ?>
                        @foreach ($listing['images'] as $image)
                            <div class="col-md-2 text-center" id="{{$image->uuid}}">
                                <img src="{{ asset('assets/img/vehicle/thumb/'.$image->filename) }}" width="100%" style="margin-bottom: 10px"/>
                                <label class="btn btn-xs btn-danger" onclick="javascript: deleteImg('{{ $image->uuid }}') "><i class="fa fa-trash"></i> Delete</label>
                            </div>
                        <?php $count_img++; ?>
                        @endforeach
                        <input type="hidden" value="{{ $count_img }}" id="count_img"/>
                        <br/><br/>
                    </div>

                    @if($count_img < 5 )
                    <div class="col-md-4">
                        <div class=" form-group input-group input-file imageWrap" style="margin-bottom: 10px">
                            <div class="form-control"></div>
                                  <span class="input-group-addon">
                                    <a class='btn btn-primary' href='javascript:;'>
                                        {{ Lang::get('app.browse_txt') }}
                                        <input type="file" name="image_1" id="image_1" onchange="$(this).parent().parent().parent().find('.form-control').html($(this).val());">
                                    </a>
                                  </span>
                        </div>
                        <span class="btn btn-success btn-sm pull-right" id="addImage" style="margin-top:20px"><i class="fa fa-plus"></i> Add Image</span>
                    </div>
                    @endif

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> {{ Lang::get('app.save_listing') }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div><!-- /.box -->
        </div>
    </div>
</section>
<style>
    .checkbox label, .radio label{
        padding-left: 0;
    }
</style>
@endsection

@section ('js_scripts')
@include('listings.partials._listings_js')
@endsection