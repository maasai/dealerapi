@extends('layouts.login')

@section('content')
<div class="login-logo">
    <b>Car</b>Dealer
</div>
<div class="login-box-body">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {!! Form::open(['url' => '/password/email']) !!}
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::input('email','email', old('email'), ['class'  =>"form-control", 'required'=>'required', 'placeholder'=>"email"]) !!}
    </div>

    <div class="form-group">
        {!! Form::Submit('Send Password Reset Link', ['class'=>"btn btn-info form-control"]) !!}
    </div>
    {!! Form::close() !!}
    <div class="form-group">
        <a href="{{ url('auth/login') }}">Back to login</a>
    </div>
</div>
@endsection
