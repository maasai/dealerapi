@extends('layouts.login')

@section('content')
<div class="login-logo">
    <b>Car</b>Dealer
</div>
<div class="login-box-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    {!! Form::open(['url' => '/password/reset']) !!}
    {!! Form::hidden('token', $token) !!}
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::input('email','email', null, ['class'  =>"form-control", 'required'=>'required', 'placeholder'=>"email"]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ['class'=>"form-control", 'placeholder'=>"password", 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password_confirmation', 'Confirm Password') !!}
        {!! Form::password('password_confirmation', ['class'=>"form-control", 'placeholder'=>"Confirm password", 'required']) !!}
    </div>
    <div class="form-group">
        {!! Form::Submit('Reset Password', ['class'=>"btn btn-info form-control"]) !!}
    </div>
    {!! Form::close() !!}
</div>
@endsection
