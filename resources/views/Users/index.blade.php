@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-user"></i> @Lang('app.users_title') </h3>
    </div>
    <div class="col-md-6">
        <h3><a class="btn btn-primary pull-right" data-toggle="ajax-modal" href="{{ route('backend.users.create') }}"><i class="fa fa-plus"></i> @Lang('app.add_btn')</a></h3>
    </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th width="10%">@Lang('app.photo')</th>
                            <th width="15%">@Lang('app.user_name_label')</th>
                            <th>@Lang('app.email_label')</th>
                            <th>@Lang('app.phone_label')</th>
                            <th>@Lang('app.username_label')</th>
                            <th>@Lang('app.action')</th>
                        </tr>
                        <tbody id="listing_body">
                        @foreach($data['users'] as $count => $user)
                        <tr>
                            <td><img src="{{ asset($user->photo != '' ? 'assets/img/users/'.$user->photo : 'assets/img/users/no-image.jpg' ) }}" class="" width="60px"/></td>
                            <td> {{ $user->name }} </td>
                            <td> {{ $user->email }} </td>
                            <td> {{ $user->phone }} </td>
                            <td> {{ $user->username }} </td>
                            <td>
                                <a class="btn btn-sm btn-primary" data-toggle="ajax-modal" href="{{ route('backend.users.edit', $user['uuid']) }}"><i class="fa fa-pencil"></i> @Lang('app.edit') </a>
                                @if(Auth::user()->uuid != $user['uuid'])
                                    {!! Form::open(array("method"=>"DELETE", "route" => ['backend.users.destroy', $user['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i>  @Lang('app.delete')</a>
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody></table>
                    <div  id="paginationWrapper" style="text-align: center">
                        {!! str_replace('/?', '?', $data['paginator']['links']) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@endsection
@section ('js_scripts')
        @include('users.partials._users_js')
@endsection