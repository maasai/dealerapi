<?php foreach($data['makes'] as $make) { ?>

<tr>
    <td>
        <div class="radio">
            <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
        </div>
    </td>
    <td><a class="xedit" data-field-name="make_name" data-url="<?php echo route('backend.makes.update', $make->uuid) ?>" data-title="Edit make Name"><?php echo $make['make_name'];  ?></a>
    </td>
    <td>
        <?php echo  Form::open(array("method"=>"DELETE", "route" => ['backend.makes.destroy', $make['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> Delete</a>'.Form::close(); ?>
    </td>
</tr>
<?php } ?>