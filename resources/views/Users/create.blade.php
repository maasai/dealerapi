@extends('layouts.modal')

@section('content')
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5>@Lang('app.add_user_title')</h5>
        </div>
        {!! Form::open(['route' => ['backend.users.store'], 'class' => 'ajax-submit']) !!}
        <div class="modal-body">
            <div class="form-group">
                {!! Form::label('name', Lang::get('app.user_name_label')) !!}
                {!! Form::text('name', null, ['class'=>'form-control required', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', Lang::get('app.email_label')) !!}
                {!! Form::text('email', null, ['class'=>'form-control required', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('phone', Lang::get('app.phone_label')) !!}
                {!! Form::text('phone', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('username', Lang::get('app.username_label')) !!}
                {!! Form::text('username', null, ['class'=>'form-control required', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password', Lang::get('app.password_label')) !!}
                {!! Form::password('password',['class'=>'form-control required', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password_confirmation', Lang::get('app.confirm_password_label')) !!}
                {!! Form::password('password_confirmation',['class'=>'form-control required', 'required']) !!}
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">@Lang('app.save')</button>
            <button type="button" data-dismiss="modal" class="btn btn-danger">@Lang('app.close')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection