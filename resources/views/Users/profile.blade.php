@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <h3><i class="fa fa-user"></i> {{ Lang::get('app.edit_profile') }}</h3>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header"></div>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                {!! Form::model($user, ['url'=> 'backend/profile','class'=> 'ajax-submit', 'files'=>'true']) !!}
                <div class="box-body table-responsive no-padding">
                    <div class="feedback col-md-12"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', Lang::get('app.user_name_label')) !!}
                            {!! Form::text('name', null, ['class'=>'form-control required', 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', Lang::get('app.email_label')) !!}
                            {!! Form::text('email', null, ['class'=>'form-control required', 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('phone', Lang::get('app.phone_label')) !!}
                            {!! Form::text('phone', null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('username', Lang::get('app.username_label')) !!}
                            {!! Form::text('username', null, ['class'=>'form-control required', 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', Lang::get('app.password_label')) !!}
                            {!! Form::password('password',['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', Lang::get('app.confirm_password_label')) !!}
                            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="photo">@Lang('app.profile_photo')</label>
                            {!! HTML::image(asset($user->photo != '' ? 'assets/img/users/'.$user->photo : 'assets/img/users/no-image.jpg'), 'photo', array('class' => 'thumbnail')) !!}<br>
                        </div>
                        <div class="col-md-5 form-group input-group input-file imageWrap" style="margin-bottom: 10px">
                            <div class="form-control"></div>
                                  <span class="input-group-addon">
                                    <a class='btn btn-primary' href='javascript:;'>
                                        {{ Lang::get('app.browse_txt') }}
                                        <input type="file" name="photo" id="photo" onchange="$(this).parent().parent().parent().find('.form-control').html($(this).val());">
                                    </a>
                                  </span>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> {{ Lang::get('app.save') }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div><!-- /.box -->
        </div>
    </div>
</section>
<style>
    .checkbox label, .radio label{
        padding-left: 0;
    }
</style>
@endsection
@section ('js_scripts')
    @include('listings.partials._listings_js')
@endsection