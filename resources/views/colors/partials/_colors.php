<?php
$counter = getCounter($data['paginator']);
foreach($data['colors'] as $color) {
?>
<tr>
    <td><?php echo $counter ?></td>
    <td>
        <div class="radio">
            <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
        </div>
    </td>
    <td><a class="xedit" data-field-name="color_name" data-url="<?php echo route('backend.colors.update', $color->uuid) ?>" data-title="Edit Color Name"><?php echo $color['color_name'];  ?></a>
    </td>
    <td>
        <?php echo  Form::open(array("method"=>"DELETE", "route" => ['backend.colors.destroy', $color['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> Delete</a>'.Form::close(); ?>
    </td>
</tr>
<?php $counter++; } ?>