<?php
$counter = getCounter($data['paginator']);
foreach($data['fuels'] as $fuel) { ?>
<tr>
    <td>
        <div class="radio">
            <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
        </div>
    </td>
    <td><a class="xedit" data-field-name="fuel_name" data-url="<?php echo route('backend.fuels.update', $fuel->uuid) ?>" data-title="Edit fuel Name"><?php echo $fuel['fuel_name'];  ?></a>
    </td>
    <td>
        <?php echo  Form::open(array("method"=>"DELETE", "route" => ['backend.fuels.destroy', $fuel['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> Delete</a>'.Form::close(); ?>
    </td>
</tr>
<?php $counter++; } ?>