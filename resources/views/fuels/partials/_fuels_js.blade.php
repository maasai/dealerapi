<script type="text/javascript">
$(function(){
    /*======================================
     CUSTOM SCRIPT TO UPDATE COLOR NAME
     ========================================*/
    $(document).on('click','.editable-submit',function(){
        var url = $('a.editable-open').attr('data-url');
        var field_name = $('a.editable-open').attr('data-field-name');
        var new_value = $(this).closest('td').find('.input-sm').val();
        if(new_value != ''){
            $.ajax({
                url: url,
                type: 'POST',
                data: {_method: 'PATCH',fuel_name:new_value},
                success: function(data){
                    dialogMessage(data, 'Success');
                },
                error: function(jqXhr){
                    var errors = jqXhr.responseJSON.errors;
                    var errorStr = '';
                    $.each( errors, function( key, value ) {
                        errorStr +=  value[0] + '<br/>';
                    });
                    dialogMessage(errorStr, 'Error');
                }
            });
        }else{
            $(this).parents('.form-group').addClass('has-error');
            return false;
        }
    });
});

</script>
