@extends('layouts.modal')

@section('content')
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5>@Lang('app.add_fuel_title')</h5>
        </div>
        {!! Form::open(['route' => ['backend.fuels.store'], 'class' => 'ajax-submit']) !!}
        <div class="modal-body">
            <div class="form-group">
                {!! Form::label('fuel_name', Lang::get('app.fuel_name')) !!}
                {!! Form::text('fuel_name', null, ['class'=>'form-control required', 'required']) !!}
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">@Lang('app.save')</button>
            <button type="button" data-dismiss="modal" class="btn btn-danger">@Lang('app.close')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection