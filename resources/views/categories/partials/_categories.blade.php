<?php
$counter = getCounter($data['paginator']);
 foreach($data['categories'] as $category) { ?>

    <tr>
        <td><?php echo $counter; ?></td>
        <td>
            <div class="radio">
                <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
            </div>
        </td>
        <td><a class="xedit" data-field-name="category_name" data-url="<?php echo route('backend.categories.update', $category->uuid) ?>" data-title="Edit Category Name"><?php echo $category['category_name'];  ?></a>
        </td>
        <td>
            <?php echo  Form::open(array("method"=>"DELETE", "route" => ['backend.categories.destroy', $category['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> Delete</a>'.Form::close(); ?>
        </td>
    </tr>
<?php $counter++; } ?>