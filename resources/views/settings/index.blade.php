@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-cogs"></i> @Lang('app.settings_title') </h3>
    </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header"></div>
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif

                @if($settings)

                {!! Form::model($settings, ['route' => ['backend.settings.update', $settings->uuid],'class'=>'ajax-submit', 'method'=>'PATCH', 'files'=>true]) !!}
                @else
                {!! Form::open(['route' => ['backend.settings.store'],'class'=>'ajax-submit', 'files'=>true]) !!}
                @endif
                <div class="box-body table-responsive no-padding">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', Lang::get('app.company_name_label')) !!}
                            {!! Form::text('name',null, ['class' => 'form-control input-sm','required', 'placeholder'=>'Company Name']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', Lang::get('app.company_description_label')) !!}
                            {!! Form::textarea('description',null, ['class' => 'form-control input-sm editor','placeholder'=>'Company Description', 'rows'=>'6']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', Lang::get('app.company_email_label')) !!}
                            {!! Form::email('email',null, ['class' => 'form-control input-sm','required','placeholder'=>'Company Email']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('address', Lang::get('app.company_address_label')) !!}
                            {!! Form::textarea('address',null, ['class' => 'form-control input-sm editor','placeholder'=>'Company Address', 'rows'=>'4']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('city', Lang::get('app.company_city_label')) !!}
                            {!! Form::text('city',null, ['class' => 'form-control input-sm','placeholder'=>'Company City']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('state', Lang::get('app.company_state_label')) !!}
                            {!! Form::text('state',null, ['class' => 'form-control input-sm','placeholder'=>'Company State']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('postal_code', Lang::get('app.company_postal_code_label')) !!}
                            {!! Form::text('postal_code',null, ['class' => 'form-control input-sm','placeholder'=>'Company Postal Code']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('telephone', Lang::get('app.company_telephone_label')) !!}
                            {!! Form::text('telephone',null, ['class' => 'form-control input-sm','placeholder'=>'Company Telephone']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('mileage_units', Lang::get('app.company_mileage_label')) !!}
                            {!! Form::select('mileage_units',['KM'=>'Kilometres', 'Miles' => 'Miles'],null, ['class' => 'form-control input-sm','required','placeholder'=>'Select Mileage Units']) !!}
                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            {!! Form::label('currency', Lang::get('app.company_currency_label')) !!}
                            {!! Form::text('currency',null, ['class' => 'form-control input-sm', 'required','placeholder'=>'Currency ']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('longitude', Lang::get('app.company_longitude_label')) !!}
                            {!! Form::text('longitude',null, ['class' => 'form-control input-sm', 'placeholder'=>'longitude ']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('latitude', Lang::get('app.company_latitude_label')) !!}
                            {!! Form::text('latitude',null, ['class' => 'form-control input-sm','placeholder'=>'Latitude']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('facebook', Lang::get('app.company_facebook_label')) !!}
                            {!! Form::text('facebook',null, ['class' => 'form-control input-sm','placeholder'=>'Facebook URL']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('twitter', Lang::get('app.company_twitter_label')) !!}
                            {!! Form::text('twitter',null, ['class' => 'form-control input-sm','placeholder'=>'Twitter URL']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('website', Lang::get('app.company_website_label')) !!}
                            {!! Form::text('website',null, ['class' => 'form-control input-sm','required','placeholder'=>'Company Website URL']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('logo', Lang::get('app.company_logo_label')) !!}
                            @if($settings && $settings->logo != '')
                            {!! HTML::image(asset('assets/img/'.$settings->logo), 'logo', array('class' => 'thumbnail')) !!}
                            @endif
                            <div class=" form-group input-group input-file" style="margin-bottom: 10px">
                                <div class="form-control"></div>
                                  <span class="input-group-addon">
                                    <a class='btn btn-primary' href='javascript:;'>
                                        {{ Lang::get('app.browse_txt') }}
                                        <input type="file" name="logo" id="logo" onchange="$(this).parent().parent().parent().find('.form-control').html($(this).val());">
                                    </a>
                                  </span>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_format', Lang::get('app.date_format_label')) !!}
                            {!! Form::select('date_format',array('d/m/Y' => date('d/m/Y'),
                            'm/d/Y' => date('m/d/Y'),
                            'Y/m/d' => date('Y/m/d'),
                            'F j, Y' => date('F j, Y'),
                            'm.d.y' => date('m.d.Y'),
                            'd-m-Y' => date('d-m-Y'),
                            'D M j Y' => date('D M j Y')), null,['class' => 'form-control input-sm','required','placeholder'=>'Date Format']) !!}
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> {{ Lang::get('app.save_settings') }}</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div><!-- /.box -->
        </div>
    </div>
</section>
@endsection