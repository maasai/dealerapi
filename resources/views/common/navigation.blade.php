<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        @if(Auth::check())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset(Auth::user()->photo != '' ? 'assets/img/users/'.Auth::user()->photo : 'assets/img/users/no-image.jpg' ) }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{  Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> @Lang('app.online')</a>
                </div>
            </div>
        @endif

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{ Lang::get('app.main_navigation') }}</li>
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-dashboard"></i> <span>{{ Lang::get('app.dashboard') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.listings.index') }}">
                    <i class="fa fa-tasks"></i> <span>{{ Lang::get('app.listings') }}</span>
                </a>
            </li>
            <li class="header">{{ Lang::get('app.inventory_header') }}</li>
            <li>
                <a href="{{ route('backend.colors.index') }}">
                    <i class="fa fa-th-large"></i> <span>{{ Lang::get('app.colors_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.categories.index') }}">
                    <i class="fa fa-ellipsis-h"></i> <span>{{ Lang::get('app.categories_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.conditions.index') }}">
                    <i class="fa fa-road"></i> <span>{{ Lang::get('app.conditions_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.features.index') }}">
                    <i class="fa fa-reorder"></i> <span>{{ Lang::get('app.features_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.fuels.index') }}">
                    <i class="fa fa-fire"></i> <span>{{ Lang::get('app.fuels_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.makes.index') }}">
                    <i class="fa fa-car"></i> <span>{{ Lang::get('app.makes_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.models.index') }}">
                    <i class="fa fa-bus"></i> <span>{{ Lang::get('app.models_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.transmissions.index') }}">
                    <i class="fa fa-wrench"></i> <span>{{ Lang::get('app.transmissions_menu') }}</span>
                </a>
            </li>
            <li class="header">{{ Lang::get('app.config_header') }}</li>
            <li>
                <a href="{{ route('backend.users.index') }}">
                    <i class="fa fa-user"></i> <span>{{ Lang::get('app.users_menu') }}</span>
                </a>
            </li>
            <li>
                <a href="{{ route('backend.settings.index') }}">
                    <i class="fa fa-cogs"></i> <span>{{ Lang::get('app.settings_menu') }}</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>