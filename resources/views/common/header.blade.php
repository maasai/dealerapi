<header class="main-header">
<!-- Logo -->
<a href="{{ url('/') }}" class="logo"><b>Signal</b>Autos</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">

<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">
<ul class="nav navbar-nav">


<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        @if(Auth::check())
            <img src="{{ asset(Auth::user()->photo != '' ? 'assets/img/users/'.Auth::user()->photo : 'assets/img/users/no-image.jpg' ) }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{  Auth::user()->name }}</span>
        @endif
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            @if(Auth::check())
                <img src="{{ asset(Auth::user()->photo != '' ? 'assets/img/users/'.Auth::user()->photo : 'assets/img/users/no-image.jpg' ) }}" class="img-circle" alt="User Image">
                <p>{{  Auth::user()->name }}</p>
            @endif
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="{{ url('backend/profile') }}" class="btn btn-default btn-flat">@Lang('app.profile') </a>
            </div>
            <div class="pull-right">
                <a href="{{ url('auth/logout') }}" class="btn btn-default btn-flat">@Lang('app.logout')</a>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
</nav>
</header>