@extends('layouts.modal')

@section('content')
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h5>@Lang('app.edit_model_title')</h5>
        </div>
        {!! Form::model($model, ['route'=> ['backend.models.update', $model->uuid], 'method' => 'PATCH', 'class'=> 'ajax-submit']) !!}
        <div class="modal-body">
            <div class="form-group">
                {!! Form::label('make_id', Lang::get('app.make_name')) !!}
                {!! Form::select('make_id', $makes, null, ['class'=>'form-control required chosen', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('model_name', Lang::get('app.model_name')) !!}
                {!! Form::text('model_name', null, ['class'=>'form-control required', 'required']) !!}
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">@Lang('app.save')</button>
            <button type="button" data-dismiss="modal" class="btn btn-danger">@Lang('app.close')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection