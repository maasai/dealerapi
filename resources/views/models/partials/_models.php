<?php $counter = getCounter($data['paginator']); ?>
<?php foreach($data['models'] as $model) { ?>

<tr>
    <td><?php echo $counter ?></td>
    <td>
        <div class="radio">
            <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
        </div>
    </td>
    <td><?php echo $model['make']['make_name'];  ?></td>
    <td><?php echo $model['model_name'];  ?></td>
    <td>
        <a class="btn btn-sm btn-primary" href=""><i class="fa fa-pencil"></i> <?php echo Lang::get('app.edit') ?> </a>
        <?php echo  Form::open(array("method"=>"DELETE", "route" => ['backend.models.destroy', $model['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> '.Lang::get('app.delete').'</a>'.Form::close(); ?>
    </td>
</tr>
    <?php $counter++ ?>
<?php } ?>