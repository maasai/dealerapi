<script type="text/javascript">
$(function(){
    /*======================================
     CUSTOM SCRIPT TO UPDATE COLOR NAME
     ========================================*/
    $(document).on('click','.editable-submit',function(){
        var url = $('a.editable-open').attr('data-url');
        var field_name = $('a.editable-open').attr('data-field-name');
        var new_value = $(this).closest('td').find('.input-sm').val();
        if(new_value != ''){
            $.ajax({
                url: url,
                type: 'POST',
                data: {_method: 'PATCH',model_name:new_value},
                success: function(data){
                    dialogMessage(data, 'Success');
                },
                error: function(jqXhr){
                    var errors = jqXhr.responseJSON.errors;
                    var errorStr = '';
                    $.each( errors, function( key, value ) {
                        errorStr +=  value[0] + '<br/>';
                    });
                    dialogMessage(errorStr, 'Error');
                }
            });
        }else{
            $(this).parents('.form-group').addClass('has-error');
            return false;
        }
    });
    /*======================================
     CUSTOM SCRIPT TO filter models
     ========================================*/
    $(document).on('click', '#filter_models', function(e){
        e.preventDefault();
        var make = $('#make_id').val();
        var model_name = $('#model_name').val();
    if(make != ''){
        $.ajax({
            url:'{{ url('backend/modelsFilter') }}',
            data: {make:make, model: model_name},
        beforeSend: function(){
            $('#filter_models').parents('.content').addClass('spinner');
        },
        success: function(data){
            $('#models_tbl').dataTable().fnDestroy();
            $('#model_tbl_body').html(data.html);
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            $('#paginationWrapper').addClass('hide');
            $('#models_tbl').dataTable();
        },
        complete:function(){
            $('#filter_models').parents('.content').removeClass('spinner');
        }
    });
    }
});
});

</script>
