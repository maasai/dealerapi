@extends('layouts.main')

@section('content')
<div class="col-md-12 content-header">
    <div class="col-md-6">
        <h3><i class="fa fa-bus"></i> @Lang('app.models_title') </h3>
    </div>
    <div class="col-md-6">
        <h3><a class="btn btn-primary pull-right" data-toggle="ajax-modal" href="{{ route('backend.models.create') }}"><i class="fa fa-plus"></i> @Lang('app.add_btn')</a></h3>
    </div>
</div>
<section class="content">
    <div class="row">
        <div class="col-xs-12" style="padding-bottom: 10px">
            <form >
                <div class="form-group col-xs-3">
                    {!! Form::label('make', 'Make') !!}
                    {!! Form::select('make_id',$makes, null, ['class' => 'form-control input-sm chosen','required','id'=>'make_id']) !!}
                </div>
                <div class="form-group  col-xs-3">
                    {!! Form::label('make', ' Model') !!}
                    {!! Form::text('model_name',null, ['class' => 'form-control','id'=>'model_name', 'placeholder'=>'Model Name']) !!}
                </div>
                <div class="form-group  col-xs-3" style="padding-top: 25px">
                    <button type="submit" class="btn btn-success" id="filter_models"> <i class="fa fa-search"></i> {{ Lang::get('app.filter_models') }}</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header"></div>
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                <div class="box-body no-padding">
                    <table class="table table-hover  dataTable" id="models_tbl">
                        <thead>
                        <tr>
                            <th width="5%">Count</th>
                            <th width="10%">
                                <div class="radio">
                                    <label for="select_all"> {!! Form::checkbox('select_all','1',null,['class'=>'minimal', 'id'=>'select_all']) !!} </label> &nbsp;
                                </div>
                            </th>
                            <th width="15%">@Lang('app.make_name')</th>
                            <th width="15%">@Lang('app.model_name')</th>
                            <th>@Lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody id="model_tbl_body">
                        <?php $counter = getCounter($data['paginator']); ?>
                        @foreach($data['models'] as $count => $model)
                        <tr>
                            <td>{{ $counter }}</td>
                            <td>
                                <div class="radio">
                                    <label for="{{$model['uuid']}}"> {!! Form::checkbox('selected',$model['uuid'],null,['class'=>'minimal select_record']) !!} </label> &nbsp;
                                </div>
                            </td>
                            <td>{{ $model->make->make_name }}</td>
                            <td>{{ $model->model_name }}</td>
                            <td>
                                <a class="btn btn-sm btn-primary" data-toggle="ajax-modal" href="{{ route('backend.models.edit', $model['uuid']) }}"><i class="fa fa-pencil"></i> @Lang('app.edit') </a>
                                {!! Form::open(array("method"=>"DELETE", "route" => ['backend.models.destroy', $model['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                                    <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> @Lang('app.delete') </a>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        <?php $counter++ ?>
                        @endforeach
                        </tbody></table>
                    <div  id="paginationWrapper" style="text-align: center">
                        {!! str_replace('/?', '?', $data['paginator']['links']) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
                    {!! Form::open(array("method"=>"DELETE", "route" => ['backend.models.destroy', ''], 'class' => 'form-inline', 'style'=>'display:inline')) !!}
                    <a class="btn btn-danger btn-sm btn-delete-multiple"><i class="fa fa-trash"></i> @Lang('app.delete_multiple') </a>
                    {!! Form::close() !!}

        </div>
    </div>
</section>
@endsection
@section ('js_scripts')
        @include('models.partials._models_js')
@endsection