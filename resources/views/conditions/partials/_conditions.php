<?php $counter = getCounter($data['paginator']);
foreach($data['conditions'] as $condition) { ?>

<tr>
    <td><?php echo $counter ?></td>
    <td>
        <div class="radio">
            <label for=""> <?php echo Form::checkbox('selected','1',null,['class'=>'minimal select_record']) ?> </label> &nbsp;
        </div>
    </td>
    <td><a class="xedit" data-field-name="condition_name" data-url="<?php echo route('backend.conditions.update', $condition->uuid) ?>" data-title="Edit Condition Name"><?php echo $condition['condition_name'];  ?></a>
    </td>
    <td>
        <?php echo  Form::open(array("method"=>"DELETE", "route" => ['backend.conditions.destroy', $condition['uuid']], 'class' => 'form-inline', 'style'=>'display:inline')).'
        <a class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i> Delete</a>'.Form::close(); ?>
    </td>
</tr>
<?php $counter++; } ?>