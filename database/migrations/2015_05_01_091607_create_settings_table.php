<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
            $table->string('uuid', 36)->primary()->unique();
            $table->string('name');
            $table->text('description');
            $table->string('email');
            $table->text('address');
            $table->string('city');
            $table->string('state');
            $table->string('postal_code');
            $table->string('telephone');
            $table->string('logo');
            $table->string('currency');
            $table->string('mileage_units');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('website');
            $table->string('date_format');

            // application extras
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
