<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('listings', function(Blueprint $table)
		{
            //makes attributes
            $table->string('uuid', 36)->primary()->unique();
            $table->string('mileage');
            $table->string('price');
            $table->string('registration_year');
            $table->string('extra_details');
            $table->boolean('featured');
            $table->boolean('active');
            $table->string('vin_number');
            $table->string('engine_size');
            $table->integer('doors');
            //Relation attributes
            $table->string('category_id', 36);
            $table->string('exterior_color_id', 36);
            $table->string('interior_color_id', 36);
            $table->string('condition_id', 36);
            $table->string('fuel_id', 36);
            $table->string('make_id', 36);
            $table->string('model_id', 36);
            $table->string('transmission_id', 36);
            // application extras
            $table->softDeletes();
            $table->timestamps();

            $table->engine = 'InnoDB';

            //Relationships
            $table->foreign('category_id')->references('uuid')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('interior_color_id')->references('uuid')->on('colors')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('exterior_color_id')->references('uuid')->on('colors')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('condition_id')->references('uuid')->on('conditions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fuel_id')->references('uuid')->on('fuels')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('make_id')->references('uuid')->on('makes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('model_id')->references('uuid')->on('vehicle_models')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('transmission_id')->references('uuid')->on('transmissions')->onDelete('cascade')->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('listings');
	}

}
