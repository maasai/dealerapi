<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehicle_images', function(Blueprint $table)
		{
            $table->string('uuid', 36)->primary()->unique();
            $table->string('filename');
            $table->string('listing_id', 36);

            // application extras
            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';

            //Relationships
            $table->foreign('listing_id')->references('uuid')->on('listings')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehicle_images');
	}

}
