<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('fuels', function(Blueprint $table)
        {
            //fuels attributes
            $table->string('uuid', 36)->primary()->unique();
            $table->string('fuel_name')->nullable();

            $table->engine = 'InnoDB';
            $table->unique('fuel_name');

            // application extras
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fuels');
	}

}
