<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMakesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('makes', function(Blueprint $table)
        {
            //makes attributes
            $table->string('uuid', 36)->primary()->unique();
            $table->string('make_name')->nullable();

            $table->engine = 'InnoDB';
            $table->unique('make_name');

            // application extras
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('makes');
	}

}
