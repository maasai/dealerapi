<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('transmissions', function(Blueprint $table)
        {
            //makes attributes
            $table->string('uuid', 36)->primary()->unique();
            $table->string('transmission_name')->nullable();

            $table->engine = 'InnoDB';
            $table->unique('transmission_name');

            // application extras
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transmissions');
	}

}
