<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleModelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehicle_models', function(Blueprint $table)
		{
            $table->string('uuid', 36)->primary()->unique();
            $table->string('model_name')->nullable();
            //relation attributes
            $table->string('make_id', 36);
            // application extras
            $table->softDeletes();
            $table->timestamps();

            $table->engine = 'InnoDB';
            //Model names are unique for every make
            $table->unique(array('model_name', 'make_id'));
            $table->index('make_id');

            //relations
            //$table->foreign('make_id')->references('uuid')->on('makes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('make_id')->references('uuid')->on('makes')->onDelete('cascade')->onUpdate('cascade');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehicle_models');
	}

}
