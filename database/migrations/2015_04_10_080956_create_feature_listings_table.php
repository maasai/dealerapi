<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureListingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feature_listings', function(Blueprint $table)
		{
            $table->string('uuid', 36)->primary()->unique();
            //relation attributes
            $table->string('feature_id', 36);
            $table->string('listing_id', 36);

            $table->engine = 'InnoDB';

            // application extras
            $table->softDeletes();
            $table->timestamps();

            //Relationships
            $table->foreign('feature_id')->references('uuid')->on('features')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('listing_id')->references('uuid')->on('listings')->onDelete('cascade')->onUpdate('cascade');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feature_listings');
	}

}
