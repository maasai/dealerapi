<?php
use Illuminate\Database\Seeder;

class OAuthScopeSeeder extends Seeder{

    public function run()
    {
        DB::table('oauth_scopes')->insert([
            'scope'       => 'basic',
            'name'        => 'Basic scope name',
            'description' => 'Basic scope description',
            'created_at'  => new DateTime,
            'updated_at'  => new DateTime,
        ]);
    }

} 