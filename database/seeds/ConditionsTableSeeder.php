<?php 
use Illuminate\Database\Seeder;

class ConditionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('conditions')->delete();


        $conditions = [
            "New",
            "Pre-Owned"
        ];

        foreach( $conditions as $condition)
        {
            \App\Condition::create([
                'condition_name'    => $condition
            ]);
        }
    }

}