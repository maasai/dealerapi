<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call('ColorsTableSeeder');
        $this->command->info('Colors table seeded!');

        $this->call('CategoriesTableSeeder');
        $this->command->info('Categories table seeded!');

        $this->call('TransmissionsTableSeeder');
        $this->command->info('Transmissions table seeded!');

        $this->call('MakesTableSeeder');
        $this->command->info('Makes table seeded!');

        $this->call('FuelsTableSeeder');
        $this->command->info('Fuels table seeded!');

        $this->call('FeaturesTableSeeder');
        $this->command->info('Features table seeded!');

        $this->call('ConditionsTableSeeder');
        $this->command->info('Conditions table seeded!');

        $this->call('VehicleModelsTableSeeder');
        $this->command->info('Vehicle Models table seeded!');

        $this->call('ListingsTableSeeder');
        $this->command->info('Listings table seeded!');

        $this->call('FeatureListingsTableSeeder');
        $this->command->info('Feature Listings table seeded!');

        $this->call('UsersTableSeeder');
        $this->command->info('Users table seeded!');

        $this->call('OAuthClientSeeder');
        $this->command->info('OAuthClient table seeded!');

        $this->call('OAuthScopeSeeder');
        $this->command->info('OAuthScope table seeded!');
	}

}
