<?php 
use Illuminate\Database\Seeder;

class VehicleModelsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('vehicle_models')->delete();

        //Benz Models
        $benzModels = ["190","190E","200", "200E", "220","220E","230E", "260E","280E","300D","300E","300TE", "500SE",            "911", "A-Class", "Actros", "Atego","Axor", "B-Class","C180","C200", "C220", "C230", "C240", "C270","C280", "C320", "C350", "CL", "CLC", "CLK", "CLS","E200","E220","E230","E240","E250", "E270", "E280","E320", "E350",            "E420", "E500", "G-Class", "GL Class","M Class","Ponton","R Class","S Class","SC Class", "SL Class", "SLK Class","SLR Class","Sprinter", "Vaneo", "Viano", "Vito"];
        $benzMake = \App\Make::where('make_name', '=', 'Benz')->get();
        foreach( $benzModels as $benzModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $benzModel,
                'make_id'       => $benzMake['0']->uuid
            ]);
        }
        //Honda models
        $hondaModels = ["1300","599","919","Accord","Acty","Airwave","ANF","Ape","Avancier","AX-1","Ballade","Benly","Big","C90","CA","Capa","CB","CD","CG","CH","City","Civic","CJ","CL","CM","CN","Concerto","CR","CR-V","Crea","CRF","CRM","Crossroad","CRX","CT","CX","Deauville","Dio","DN-01","Dream","Dylan","Element","Elite","Elysion","Express","F","FES","Fit","FJR","FMX","Foresight","Forza","FourTrax","FR-V","FSC","FT","FTR","Fuya","GB","GL","Goldwing","Gorilla","Gyro","H","Helix","Hornet","HR-V","Integra","Interceptor","Jazz","Lead","Legend","Life","Magna","MB5","Metropolitan","Millenium","Mobilio","Monkey","MT","MTX","Nighthawk","NR","NS","NSR","NSS","NSX","NT","NTV","NX","Odyssey","Pantheon","Partner","Passport","PC","Pilot","Prelude","PS","PX50","Quintet","RC","Rebel","Reflex","Ridgeline","Ruckus","Rune","RVF","RVT","S-600","S-MX","S-Wing","S2000","S500","S800","SCV","SFX","SH","Shadow","Shuttle","SilverWing","Sky","SL","SLR","Smart","SSM","ST","Steed","Stepwagon","Stream","SW-T400","That","Tiger","TL","Today","Torneo","TRX","V65","Valkyrie","Vamos","Varadero","VFR","Vigor","VT","VTX","X-Eleven","X4","X8R-S","XBR","XL","XR","Z","Z50R","Zoomer"];
        $hondaMake = \App\Make::where('make_name', '=', 'Honda')->get();
        foreach( $hondaModels as $hondaModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $hondaModel,
                'make_id'       => $hondaMake['0']->uuid
            ]);
        }

        //Isuzu models
        $isuzuModels = ["117","Amigo","Ascender","Axiom","Bellet","Bighorn","Como","CXZ","D-MAX","Direct","ELF Truck","ELF Van","FFR","Florian","Forward","Frontera","FRR","FSR","FTR","FVR","FVX","FVZ","Gemini","GIGA","Hombre","I-Series","Impulse","Kai","KB","Minx","New","NKR","NPR","NQR","Piazza","Rodeo","Stylus","Succeed","TFR54","Tougher","Trooper","VehiCross","Wizard","ZXS"];
        $isuzuMake = \App\Make::where('make_name', '=', 'Isuzu')->get();
        foreach( $isuzuModels as $isuzuModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $isuzuModel,
                'make_id'       => $isuzuMake['0']->uuid
            ]);
        }

        //LandRover Models
        $landRoverModels = ["109","110","88","Defender","Discovery","Freelander","I","II","III","Range Rover","Range Rover Evoque"];
        $landRoverMake = \App\Make::where('make_name', '=', 'LandRover')->get();
        foreach( $landRoverModels as $landRoverModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $landRoverModel,
                'make_id'       => $landRoverMake['0']->uuid
            ]);
        }

        //Mazda Models
        $mazdaModels = ["1000","110","121","2","3","323","5","6","616","626","787","818","929","Atenza","Axela","AZ","B","Bongo","BT-50","Capella","Carol","Chante","Cosmo","CX-7","CX-9","Demio","Drifter","Etude","Eunos","Familia","Lantis","Laputa","Levante","Luce","Marathon","Midge","Montrose","MPV","MS-6","MS-8","MS-9","MX-3","MX-5","MX-6","Persona","Premacy","Protege","R-100","R360","Rustler","RX-2","RX-3","RX-4","RX-7","RX-8","Sentia","Spiano","Titan Dash","Tribute","Verisa","Washu","Xedos"];
        $mazdaMake = \App\Make::where('make_name', '=', 'Mazda')->get();
        foreach( $mazdaModels as $mazdaModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $mazdaModel,
                'make_id'       => $mazdaMake['0']->uuid
            ]);
        }

        //Mitsubishi Models
        $mitsubishiModels = ["3000","Airtrek","ASX","Canter","Carisma","Celeste","Challenger","Chariot","Colt","Debonair","Delica","Diamante","Dingo","Dion","Dyna Truck","Eclipse","EK","Endeavor","FH","Fighter","FTO","Fuso","Galant","Grandis","GTO","Jeep","L200","Lancer / Cedia","Lancer Evo","Legnam","Magna","Minica","Minicab Truck","Mirage","Outlander","Pajero","Pajero IO","Pajero Mini","Raider","Rosa","RPM","RVR","Sapporo","Shakti MT180D","Shogun","Sigma","SpaceRunner","Spacestar","Spacewagon","Spyder","Super Great","Verada","Warrior"];
        $mitsubishiMake = \App\Make::where('make_name', '=', 'Mitsubishi')->get();
        foreach( $mitsubishiModels as $mitsubishiModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $mitsubishiModel,
                'make_id'       => $mitsubishiMake['0']->uuid
            ]);
        }

        //Nissan Models
        $nissanModels = ["100","1400","200SX","240SX","260Z","300ZX","350Z","370Z","Advan","Almera","Altima","Armada","Atlas","Avenir","Basara","Bluebird","Caravan","Carina","Cedric","Cefiro","Cherry","Cima","Civilian","Commercial","Crew","Cube","DoubleCab","Dualis","E20","Elgrand","Fairlady","FB14","FB15","Frontier","Fuga","Fusion","Gloria","Grand","GT-R","Hardbody","HyperMini","Ideo","Interstar","Lafesta","Laurel","Leopard","Liberty","Livina","March","Maxima","Micra","Moco","Murano","Navara","Note","NP300","Pathfinder","Patrol","Pick-Up","Pintara","Platina","Prairie","Presage","Presea","President","Primera","Pulsar","Qashqai","Quest","Rasheen","Rogue","Sahara","Santana","Sentra","Serena","Silvia","Skyline","Stagea","Stanza","Sunny","Teana","Terrano","Tiida","Titan","Trailrunner","UA-QG","UD","Urvan","Vanette","Versa","Violet","Wingroad","X-Trail","Xterra"];
        $nissanMake = \App\Make::where('make_name', '=', 'Nissan')->get();
        foreach( $nissanModels as $nissanModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $nissanModel,
                'make_id'       => $nissanMake['0']->uuid
            ]);
        }

        //Subaru Models
        $subaruModels = ["1.8","1400","1600","1800","360","Alfa","Baja","DL","FF1","Forester","Impreza","Justy","Legacy","Leone","Liberty","Mini","Outback","Pleo","R2","Rex","STX","SVX","Traviq","Tribeca","Vivio","XV"];
        $subaruMake = \App\Make::where('make_name', '=', 'Subaru')->get();
        foreach( $subaruModels as $subaruModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $subaruModel,
                'make_id'       => $subaruMake['0']->uuid
            ]);
        }

        //Toyota Models
        $toyotaModels = ["1000","4-Runner","Allex","Allion","Alphard","Altezza","Aristo","Auris","Avalon","Avanza","Avensis","Aygo","bB","Belta","Blade","Blizzard","Brevis","Caetano","Caldina","Cami","Camry","Carib","Carina","Celica","Celsior","Chaser","Coaster","Condor","Conquest","Corolla","Corona","Corsa","Cressida","Cresta","Crown","Cynos","Denso","Duet","Dyna","ES","Estima","FCHV","Fielder","FK","Fortuner","Fun Cargo","FXS","Gaia","Grand Hiace","Granvia","GT1","Harrier","HiAce","Highlander","Hilux","HMV","Ipsum","ISIS","IST","Kluger","Land Cruiser","Land Cruiser Prado","Lexcen","Lite-Ace","Mark II","Mark X","Master","Matrix","MR-S","MR2","Nadia","Noah","Opa","Paseo","Passo","Picnic","Platz","Pod","Porte","Premio","Previa","Prius","Probox","Progress","Publica","Quantum","Ractis","Raum","RAV4","Regius Van","Retro","RSC","Run-X","Rush","SA","Sequoia","Sera","Sienna","Sienta","Soarer","Solara","Spacio","Sparky","Sprinter","Stallion","Starlet","Succeed","Supra","Surf","Tacoma","Tazz","Tercel","Townace","Toyoace","TRD","Tundra","Vanguard","Venture","Venza","Verossa","Vios","Vista","Vitz","Voltz","Voxy","Will","Windom","Wish","XYR"];
        $toyotaMake = \App\Make::where('make_name', '=', 'Toyota')->get();
        foreach( $toyotaModels as $toyotaModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $toyotaModel,
                'make_id'       => $toyotaMake['0']->uuid
            ]);
        }

        //Volkswagen Models
        $volkswagenModels = ["1","1302","1303","1500","1600","181","411","AAC","Beetle","Bora","Cabriolet","Caddy","Caravelle","CC","Citi","Commercial","Concept","Corrado","Derby","Eos","Eurovan","Fox","Fusca","GLI","Golf","Hac","Iltis","Jetta","K","K70","Karmann-Ghia","Kombi","LT","Lupo","Magellan","Microbus","Multivan","Parati","Passat","Phaeton","Pickup","Polo","Quantum","Rabbit","Routan","Santana","Schwimmwagen","Scirocco","Sharan","SP2","Tiguan","Touareg","Touran","Transporter","Vento"];
        $volkswagenMake = \App\Make::where('make_name', '=', 'Volkswagen')->get();
        foreach( $volkswagenModels as $volkswagenModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $volkswagenModel,
                'make_id'       => $volkswagenMake['0']->uuid
            ]);
        }

        //Bmw Models
        $bmwModels = ["X5","M3","E24","Z4 Roadster","3 Series","5 Series","i8","7 series","X1","X3","X6","i3","X4","Z4","6 Series","4 Series","M6","M5","M4","2 series","M235","X5M","X6M","ALPINA B6 gran coupe","ActiveHybrid 7","1series","8series","Z3","Z8","M"];
        $bmwMake = \App\Make::where('make_name', '=', 'Bmw')->get();
        foreach( $bmwModels as $bmwModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $bmwModel,
                'make_id'       => $bmwMake['0']->uuid
            ]);
        }

        //Hyundai Models
        $hyundaiModels = ["Santa fe","Elantra","Accent","Tucso","Genesis","Sonata","Veloster","Azela","Aquus","Santa fe sport","Elantra GT","Santa hybrid","Elantra touring","Veracruz","Tiburon","Excel","XG350","Entourage","XG300","Scoupe", "Elite i20","Xcent","i20 active","4S fluidic verna","ix25"];
        $hyundaiMake = \App\Make::where('make_name', '=', 'Hyundai')->get();
        foreach( $hyundaiModels as $hyundaiModel)
        {
            \App\VehicleModel::create([
                'model_name'    => $hyundaiModel,
                'make_id'       => $hyundaiMake['0']->uuid
            ]);
        }
    }

}