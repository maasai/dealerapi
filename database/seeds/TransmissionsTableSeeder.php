<?php
use Illuminate\Database\Seeder;

class TransmissionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('transmissions')->delete();

        $transmissions = [
            "Automatic",
            "Manual"
        ];

        foreach( $transmissions as $transmission)
        {
            \App\Transmission::create([
                'transmission_name'    => $transmission
            ]);
        }
    }

}