<?php
use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('features')->delete();

        $features = [
            "All Wheel Drive",
            "Air Conditioning",
            "CD Player",
            "Fog Lights",
            "Side Airbags",
            "Keyless Entry",
            "Leather Seats",
            "External Winch",
            "Power Steering",
            "Electric Mirrors",
            "AM/FM Radio",
            "Turbo Charged",
            "Electric Windows",
            "Tinted Windows",
            "Wheel Locks",
            "Spotlight",
            "Rear Camera",
            "Cup Holders",
            "Alloy Wheels",
            "Spoilers",
            "Traction Control",
            "Front Fog Lamps",
            "Bullbar",
            "Sunroof",
            "Xenon Lights",
            "Roof Rack"
        ];

        foreach( $features as $feature)
        {
            \App\Feature::create([
                'feature_name'    => $feature
            ]);
        }
    }

}