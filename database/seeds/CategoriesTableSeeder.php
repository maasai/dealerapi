<?php
use Illuminate\Database\Seeder;


class CategoriesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->delete();

        $categories = [
            "Saloons",
            "SUVs",
            "4 Wheel Drives",
            "Farm Machinery & Tractors",
            "Wagon",
            "Buses & Vans",
            "Trucks",
            "Motor Cycles",
            "Pick- Ups",
            "Other"
        ];

        foreach( $categories as $category)
        {
            \App\Category::create([
                'category_name'    => $category
            ]);
        }
    }

}