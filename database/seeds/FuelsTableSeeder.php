<?php
use Illuminate\Database\Seeder;

class FuelsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('fuels')->delete();

        $fuels = [
            "Diesel",
            "Gasoline"
        ];

        foreach( $fuels as $fuel)
        {
            \App\Fuel::create([
                'fuel_name'    => $fuel
            ]);
        }
    }

}