<?php
use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('colors')->delete();

        $colors = [
            "Yellow",
            "White",
            "Silver",
            "Red",
            "Maroon",
            "Purple",
            "Pink",
            "Orange",
            "Grey",
            "Dark Grey",
            "Green",
            "Dark Green",
            "Gold",
            "Brown",
            "Blue",
            "Dark Blue",
            "Black",
            "Other",
        ];

        foreach( $colors as $color)
        {
            \App\Color::create([
                'color_name'    => $color
            ]);
        }
    }

}