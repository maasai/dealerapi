<?php
use Illuminate\Database\Seeder;

class MakesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('makes')->delete();

        $makes = [
            "Hyundai",
            "Suzuki",
            "BMW",
            "Benz",
            "Toyota",
            "Nissan",
            "Isuzu",
            "Honda",
            "Mitsubishi",
            "Subaru",
            "Volkswagen",
            "Mazda",
            "LandRover"
        ];

        foreach( $makes as $make)
        {
            \App\Make::create([
                'make_name'    => $make
            ]);
        }
    }

}