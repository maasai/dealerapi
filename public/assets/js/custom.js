﻿$(document).ready(function () {
    $('form').validator();
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    $('.editor').wysihtml5({image:false,link:false});
    $(document).ajaxStart(function() { Pace.restart(); });
    $("select.chosen").chosen({ width: '100%' });

    /*======================================
     CUSTOM SCRIPT TO SUBMIT A FORM VIA AJAX
     ========================================*/
    $(document).on('submit', '.ajax-submit', function(e){
        e.preventDefault();

        var $form = $(this);
        var formData = new FormData(this);

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend : function(){
                $form.addClass('spinner');
            },
            success : function(){
                window.location.reload();
            },
            error : function(jqXhr, json, errorThrown){
                var errors = jqXhr.responseJSON.errors;
                $.each( errors, function( key, value ) {
                    $('input[name="'+key+'"]').parents('.form-group').addClass("has-error");
                    $('select[name="'+key+'"]').parents('.form-group').addClass("has-error");
                    if(key  == 'features'){
                        $('#features_error').html('Please select atleast one feature').removeClass('hide');
                    }
                });
            },
            complete : function(){
                $form.removeClass('spinner');
            }
        });
    });
    /*======================================
     CUSTOM SCRIPT TO DO AJAX PAGINATION
     ========================================*/
    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        var anchor = $(this);
        var page = anchor.attr('href').split('page=')[1];

        $.ajax({
            url:'?page='+ page,
            beforeSend: function(){
                $('#paginationWrapper').parents('.box-body').addClass('spinner');
            },
            success: function(data){
                $('#listing_body').html(data.html);
                $('#paginationWrapper').html(data.paginator);

                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
                $('input[name="select_all"]').iCheck('uncheck');
                $('.xedit').editable();
            },
            complete:function(){
                $('#paginationWrapper').parents('.box-body').removeClass('spinner');
            }
        })
    });
    /*======================================
     CUSTOM FUNCTION TO SELECT ALL CHECKBOXES
     ========================================*/
    $(document).on('ifChecked ifUnchecked', 'input[name="select_all"]', function(event) {
        if (event.type == 'ifChecked') {
            $('.select_record').iCheck('check');
            $('#label_select_all').html('Deselect All');
        } else {
            $('.select_record').iCheck('uncheck');
            $('#label_select_all').html('Select All');
        }
    });

   $(document).on('ifChanged', $('.select_record'), function(){
       if($('.select_record').filter(':checked').length == $('.select_record').length) {
           $('input[name="select_all"]').prop('checked', 'checked');
           $('#label_select_all').html('Deselect All');
       } else {
           $('input[name="select_all"]').removeProp('checked');
           $('#label_select_all').html('Select All');
       }
       $('input[name="select_all"]').iCheck('update');
   });

    /*======================================
     CUSTOM FUNCTION ADD MORE IMAGES
     ========================================*/
    $(document).on('click', '#addImage', function(event) {

        if ($('#count_img').length) {
           var existing_img = $('#count_img').val();
        }else{
            var existing_img = 0;
        }
        var new_elements = $(".imageWrap:last").clone();
        var numItems = $('.imageWrap').length;
        if(numItems+parseInt(existing_img) < 5){
            var new_item_num = parseInt(numItems + 1);

            new_elements.find('input').each(function() {
                var element_name = $(this).attr('name', 'image_'+new_item_num);
                var new_id = $(this).attr('id', 'image_'+new_item_num);
            });
            $('#addImage').before(new_elements);
        }
    });
    /*-----------------------------------------------------------
     Delete Button clicked
     --------------------------------------------------------------*/
    $(document).on('click', '.btn-delete', function (e){e.preventDefault(); confirm_dialog($(this).parent('form')); });
    function confirm_dialog(form){
        BootstrapDialog.show({
            title: 'Deleting a Record',
            message: 'You are about to delete a record. This action cannot be undone. Do you want to proceed?',
            buttons: [ {
                icon: 'fa fa-check',
                label: ' Yes',
                cssClass: 'btn-success btn-sm',
                action: function(){
                    form.submit();
                }
            }, {
                icon: 'fa fa-remove',
                label: 'No',
                cssClass: 'btn-danger btn-sm',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
        return false;
    }
    /*-----------------------------------------------------------
     Delete multiple Button clicked
     --------------------------------------------------------------*/
    $(document).on('click', '.btn-delete-multiple', function (e){e.preventDefault(); confirm_multiple_dialog($(this).parent('form')); });
    function confirm_multiple_dialog(form){
        var selected = $('.select_record').filter(':checked').length;
        if(selected  > 0){
            BootstrapDialog.show({
                title: 'Deleting a Record',
                message: 'You are about to delete records. This action cannot be undone. Do you want to proceed?',
                buttons: [ {
                    icon: 'fa fa-check',
                    label: ' Yes',
                    cssClass: 'btn-success btn-sm',
                    action: function(dialogItself){
                        var url = form.attr('action');
                        var token = form.find('input[name="_token"]').val();
                        var _method = 'DELETE';
                        var selected_rows = [];
                        $('input[name="selected"]:checked').each(function() {
                            selected_rows.push(this.value);
                        });

                        $.post(url+'/'+'delete_multiple', {
                            items: JSON.stringify(selected_rows), _method: _method,  token:token
                        },
                         function(data) {
                            var response = data.responseJSON;
                            if (data.success)
                            {
                                window.location.reload();
                            }
                        });
                        dialogItself.close();
                    }
                }, {
                    icon: 'fa fa-remove',
                    label: 'No',
                    cssClass: 'btn-danger btn-sm',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        }
        else{
            BootstrapDialog.show({
                title: 'Select Record to delete',
                message: 'Please select at least one record to delete',
                buttons: [ {
                    icon: 'fa fa-check',
                    label: ' OK',
                    cssClass: 'btn-success btn-sm',
                    action: function(dialogItself){
                        dialogItself.close();
                    }
                }]
            });
        }
        return false;
    }
    /*-----------------------------------------------------------
     X-editable plugin
     --------------------------------------------------------------*/
    $('.xedit').editable();

    /*-----------------------------------------------------------
     Modal displays
     --------------------------------------------------------------*/
    var $modal = $('#ajax-modal');

    $(document).on('click', '[data-toggle="ajax-modal"]', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.get(url, function(data) {
            $modal.modal();
            $modal.html(data);
            $("select.chosen").chosen({ width: '100%' });
            $('form').validator();
        });
    });
});

function dialogMessage(message, title){
    BootstrapDialog.show({
        title: title,
        message: message,
        buttons: [ {
            icon: 'fa fa-check',
            label: ' OK',
            cssClass: 'btn-success btn-sm',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
}




      